// 定义搜索界面的接口信息
import axios from "../http"

// 查询热搜关键词
export const getHotWords = params => axios({
    url: '/search/hot',
    params
})


// 查询结果集的列表
export const getSearchRes = params => axios({
    url: '/cloudsearch',
    params
})