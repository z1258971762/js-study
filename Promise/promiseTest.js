// Promise 的基本测试
// 导入我们自己的Promise实例
// const { resolve } = require("./Promise.js");
const Promise = require("./Promise.js");

// 编写测试方法
// const p = new Promise((resolve, reject) => {
//     // 执行一个异步函数试试
//     setTimeout(() => {
//         let radom = Math.random();
//         console.log("这是异步的", radom);
//         if (radom > 0.5) resolve("> 0.5");
//         else reject("< 0.5");
//     }, 1000);

//     // resolve("OK");
//     // reject("bad");
//     // throw new Error("出错了");
// });

// // 执行then函数
// p.then(
//     (succdata) => {
//         console.log("succ", succdata);
//     },
//     (rejectData) => {
//         console.log("fail", rejectData);
//     }
// );
// p.then(
//     (res) => {
//         console.log("alalla", res);
//     },
//     (res) => {
//         console.log("dididi", res);
//     }
// );
// p.then(
//     (succdata) => {
//         console.log("succ", succdata);
//     },
//     (rejectData) => {
//         console.log("fail", rejectData);
//     }
// );



// 下面测试then的链式调用
// const pThen = new Promise((resolve, reject) => {
//     resolve("hello then");
// });

// let then1ret = pThen.then(
//     (succ) => {
//         console.log("phen1-succ:", succ);
//         // 测试循环引用
//         // return then1ret;
//         // 测试返回值
//         return 111;
//     },
//     (fail) => {
//         console.log("pthen1-fail:", fail);
//     }
// );
// then1ret.then(
//     (succ) => {
//         console.log("phen2-succ:", succ);
//         throw '出错了'
//     },
//     (fail) => {
//         console.log("pthen2-fail:", fail);
//     }
// ).then(
//     (succ) => {
//         console.log("phen3-succ:", succ);
//     },
//     (fail) => {
//         console.log("pthen3-fail:", fail);
//         return 123
//     }
// ).catch((e)=>{
//     console.log('catch',e);
// });


// 测试finally啥的
// let p3 = new Promise((resolve,reject)=>{
//     resolve(123);
// }).then((res)=>{
//     console.log('res1',res);
//     throw 123
// }).finally((val)=>{
//     console.log('finally111',val);
// }).then((val)=>{
//     console.log('finall-then-succ',val);
// },(rso)=>{
//     console.log('finall-then-fail',rso);
// })

// 测试Promise.all
// let promise1 = Promise.reject('error---lalala');
// let promise2 = Promise.resolve('123');
// let promise3 = Promise.resolve('456');

// Promise.all([promise3,promise1,promise2]).then((data)=>{
//     console.log('data',data);
// }).catch(e=>{console.log('error',e)});


// 测试Promise.race