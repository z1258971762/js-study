import {createRouterMap} from './create-route-map'
import createRoute from './util/route';
/**
 * 
 * @param {路由规则数组} routes 
 * @param {路由实例} router 
 */
export function createMatcher(routes,router){

    const {pathList,pathMap,nameMap}  = createRouterMap(routes);

    // 根据地址找到匹配的路由
    function match(path){
        const record = pathMap[path];
        if(record){
            return createRoute(record,path);
        }else{
            return createRoute(null,path);
        }
    }
    // 动态添加路由
    function addRoutes(routes){
        return createRouterMap(routes,pathList,pathMap)
    }

    return {
        match,
        addRoutes
    }
}