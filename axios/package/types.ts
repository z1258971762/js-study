import type { AxiosRequestConfig, AxiosResponse } from "axios";

export interface RequestInterceptors {
    // 请求拦截器
    requestInterceptors?: (config: AxiosRequestConfig) => AxiosRequestConfig
    requestInterceptorsCatch?: (err: any) => any

    // 响应拦截器----增加范型，指定返回值类型
    responseInterceptors?: <T = AxiosResponse>(res: T) => T
    responseInterceptorsCatch?: (err: any) => any
}

// 自定义请求类型，支持传入实例拦截器
export interface RequestConfig extends AxiosRequestConfig {
    interceptiors?: RequestInterceptors
}

// 自定义取消请求的数据类型
export interface CancelRequestSource {
    [index: string]: (reson: string) => void
}