// pages/netTest/netTest.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  sendGet(){
    console.log('get 请求');
    wx.request({
      url: 'https://baidu.com',
      method:'GET',
      data: {
        a:100,
        b:200
      },
      success(res){
        console.log('get ret：' ,res.data);
      },
      fail(err){
        console.log('get fail: ',err);
      },
      complete(){
        console.log('无论成功失败都会执行');
      }
    })
  },

  sendPost(){
    wx.request({
      url: 'https://baidu.com',
      method:'POST',
      data: {
        a:100,
        b:200
      },
      success(res){
        console.log('get ret：' ,res.data);
      },
      fail(err){
        console.log('get fail: ',err);
      },
      complete(){
        console.log('无论成功失败都会执行');
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})