import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "@/mobile/flexible" // 适配

// 引入vant
import {
  Tabbar,
  TabbarItem,
  NavBar,
  Col,
  Row,
  Image as VanImage,
  Cell,
  Icon,
  Search,
  List,
  Progress
} from "vant";
import "@/styles/reset.css";
import "@/mobile/flexible"; // 适配

Vue.config.productionTip = false;

// 使用组件
Vue.use(Tabbar);
Vue.use(List)
Vue.use(Search)
Vue.use(Icon)
Vue.use(Cell)
Vue.use(VanImage)
Vue.use(Col)
Vue.use(Row)
Vue.use(TabbarItem)
Vue.use(NavBar)
Vue.use(Progress)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
