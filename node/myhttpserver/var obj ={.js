// 箭头函数的this指向
function func() {
    var value = 2;
    var fun1 = () => { console.log(this.value); }
    var obj = {
        a: {
            b: function () {
                console.log(this.value);
            },
            c: () => {
                console.log(this.value);
            },
            d: function () {
                return () => {
                    console.log(this.value);
                }
            },
            e: fun1,
            value: 1
        },
        value: 0
    }


    obj.a.b();
    obj.a.c();
    obj.a.d()();
}

var obj2 = {
    fun: func,
    value:100
}

func()
obj2.fun();