const http = require('http');
const url = require('url');
const queryString = require('queryString');
// 解析之后是个对象
console.log(queryString.parse('a=1&b=2'));

let server = http.createServer((req, res) => {
    console.log('received');
    // 请求方式
    console.log(req.method);
    // 请求地址
    console.log(req.url);
    // 用URL解析请求地址。后面的true表示完全解析，会把返回query转换为Object
    // query拿到的是get请求的参数
    console.log(url.parse(req.url, true));
    // 路径和参数解构出来
    let { pathname, query } = url.parse(req.url, true);
    // 获取请求头
    console.log(req.headers);


    // post 请求要基于发布订阅的方式
    // request是一个可读流
    let arr = [];
    req.on('data', (chunk) => {
        arr.push(chunk);
    });

    // 结束
    req.on('end', () => {
        console.log('body:', Buffer.concat(arr).toString());
    })

    // 服务器返回值

    // 服务器状态吗
    res.statusCode = 200;
    // 设置响应头
    res.setHeader('Content-type', 'text/plain;charset=utf-8');
    // 自定义头
    res.setHeader('abc', 'abc');
    // response是可写流
    res.write('OK');
    // 请求结束，向前端返回结果
    res.end('请求结束');

    // 服务代码主线程是同步的，所以尽量少在这里写同步代码，尽量写异步的
})

let port = 3000;
server.listen(port, () => {
    console.log('server start');
})

server.on('error', (err) => {
    if (err.code == 'EADDRINUSE')
        server.listen(++port);
});


// 发送get请求
http.get({
    port: 3000,
    host: 'localhost',
    pathname: '/id?name=123'
}, (res) => {
    // res 是可读流
    let arr = [];
    res.on('data', (chunk) => {
        arr.push(chunk);
    })
    res.on('end', () => {
        console.log(Buffer.concat(arr).toString());
    })
})

// 发送post请求
let client = http.request({
    port: 3000,
    hostname: 'localhost',
    method: 'post',
    path:'/'
}, (res) => {
    let arr = [];
    res.on('data', (chunk) => {
        arr.push(chunk);
    })
    res.on('end', () => {
        console.log(Buffer.concat(arr).toString());
    })
})
// 这个要写，不然结束不了
// 这里是个坑，注意node版本，版本太低的时候，服务器是收不到回应的
client.end('hello');