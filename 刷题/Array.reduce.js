// 请补全JavaScript代码，要求实现Array.reduce函数的功能且该新函数命名为"_reduce"。
// arr.reduce(callback(accumulator, currentValue[, index[, array]])[, initialValue])
const reduce = function (cb, init) {
    if (typeof cb != 'function') {
        throw new Error(`${cb} is not a function`);
    }
    // 累加器
    let accumulator = init || this[0];
    let currentIndex = init ? 0 : 1;
    for (currentIndex; currentIndex < this.length; currentIndex++) {
        accumulator = cb(accumulator, this[currentIndex], currentIndex, this);
    }
    return accumulator;
}
Array.prototype._reduce = reduce;

console.log([1, 2, 3]._reduce((left, right, index) => {
    console.log('l', left);
    console.log('r', right);
    console.log('i', index);
    return left + right
}
,2));
console.log('--------------');
console.log([1, 2, 3].reduce((left, right, index) => {
    console.log('l', left);
    console.log('r', right);
    console.log('i', index);
    return left + right
}
,2));