let $ = Jquery = (function (window) {
    let jquery = function (selector) {
        this.nodes = document.querySelectorAll(selector);
    }

    jquery.prototype = {
        // 封装的好处是什么？同时修改的时候方便
        each: function (callback) {
            for (let i = 0; i < this.nodes.length; i++) {
                callback.call(this, this.nodes[i], i)
            }
        },
        addClass(classes) {
            let className = classes.split(' ');
            className.forEach(value => {
                this.each(function (obj) {
                    obj.classList.add(value);
                })
            })
        },
        setText(text) {
            this.each(function (obj) {
                obj.textContent = text;
            })
        }
    }
    // 要把函数暴露出去，才能传进来selector
    return function (selector) {
        return new jquery(selector);
    }
})(window)