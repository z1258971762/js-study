// 封装我们自己的Promise
/*定义Promise的三种状态*/
//等待态
const PENDDING = "PENDDING";
//成功态
const FULFILLED = "FULFILLED";
//失败态
const REJECT = "REJECT";

/**
 * 解析Promise与上一个Promise返回值的关系，决定这个Promise的调用
 * @param {目标Promise} promise
 * @param {上一个promise的返回值} ret
 * @param {目标Promise的resolve} resolve
 * @param {目标Promise的reject} reject
 */
function analysePromise(promise, ret, resolve, reject) {
    // 判断是不是同一个实例，同一个实例就报错。循环引用，相当于是在自己等自己。
    if (promise == ret) {
        throw new Error(" circule reference  ....");
    }
    let called = false;
    // 判断一下是不是对象，是不是Promise，如果是Promise，就调用一下，然后根据它的返回值决定调用的结果
    if ((typeof ret == "object" && ret != null) || typeof ret == "function") {
        try {
            // 取对象的then属性,有就有，没有就undefined
            let then = ret.then;
            if (typeof then == "function") {
                then.call(
                    ret,
                    (y) => {
                        if (called) return;
                        called = true;
                        // 这里返回的Y可能还是一个Promise，所以就递归调用
                        analysePromise(promise, y, resolve, reject);
                    },
                    (r) => {
                        if (called) return;
                        called = true;
                        reject(r);
                    }
                );
            } else {
                // 不是then函数，则是一个普通值，直接成功
                if (called) return;
                called = true;
                resolve(ret);
            }
        } catch (e) {
            if (called) return;
            called = true;
            reject(e);
        }
    } else {
        // 普通值
        if (called) return;
        called = true;
        resolve(ret);
    }
}

/**
 * 判断传入的对象是不是Promise，有可能是个普通值呢
 * @param {promise} value 
 */
function isPromise(value) {
    if ((typeof value == 'object' && value != null) || typeof value == 'function') {
        if (typeof value.then == 'function') {
            return true;
        }
        return false;
    }
}
/**
 * 自定义Promise类
 */
class Promise {
    constructor(exector) {
        //保存当前Promise的状态
        this.status = PENDDING;
        //成功回调的参数
        this.succData = undefined;
        //失败回调的参数
        this.failData = undefined;
        //成功回调的集合
        this.succCallBacks = [];
        //失败回调的集合
        this.failCallBack = [];

        //定义成功函数和失败函数
        let resolve = (data) => {
            // 更改当前promise的状态，因为状态一旦改变不可更改，所以要加一个判断
            if (this.status == PENDDING) {
                this.status = FULFILLED;
                //保存成功的值
                this.succData = data;
                //我们在这里调用异步的回调函数
                this.succCallBacks.forEach((fun) => fun());
            }
        };
        let reject = (data) => {
            //将状态改为失败
            if (this.status == PENDDING) {
                this.status = REJECT;
                //保存失败的值
                this.failData = data;
                //我们在这里调用异步的回调函数
                this.failCallBack.forEach((fun) => fun());
            }
        };
        //这里用try catch包一下，报错了就走reject
        try {
            // 执行executor函数
            exector(resolve, reject);
        } catch (error) {
            reject(error);
        }
    }
    /**
     * 定义每个实例的then函数,接受两个参数，成功回调，和失败回调
     * @param {成功回调} onFulfill 
     * @param {失败回调} onReject 
     * @returns promise
     */
    then(onFulfill, onReject) {
        // console.log('执行了then',this);
        // 传入的onFulfill, onReject如果不是一个函数，那就把它变成函数
        onFulfill = typeof onFulfill == 'function' ? onFulfill : v => v;
        onReject = typeof onReject == 'function' ? onReject : e => { throw e };
        /**
         * 为了实现链式调用，那么每次then的返回值都应该是一个Promise
         * 链式调用的原则：
         * 上一次then的返回值，如果是一个Promise实例，则此promise的状态会决定接下来then的回调调用
         * 上一次then的返回值，如果是一个普通值（包括undefined）则会走下一个then的成功回调，并且把值传递到成功回调
         * 上一次then抛出异常，则调用下一个then的失败回调。
         */
        let p2 = new Promise((resolve, reject) => {
            //这里要做什么呢？ 这里要做的是成功就执行成功回调，失败就失败回调，如果是个异步函数，那这里就得实现一个等待的效果
            if (this.status == FULFILLED) {
                // 这里为什么要放到定时器里面？因为这个时候，我们拿不到P2，所以要异步取，否则就会报错
                setTimeout(() => {
                    try {
                        let ret = onFulfill(this.succData);
                        analysePromise(p2, ret, resolve, reject);
                    } catch (e) {
                        reject(e);
                    }
                }, 1000);
            }
            if (this.status == REJECT) {
                setTimeout(() => {
                    try {
                        let ret = onReject(this.failData);
                        analysePromise(p2, ret, resolve, reject);
                    } catch (e) {
                        reject(e);
                    }
                }, 1000);
            }
            if (this.status == PENDDING) {
                //这里呢，就得把这个函数保存起来了，因为这个时候还不知道到底是成功还是失败
                // 那等待态的回调要什么时候才能执行呢？
                // 那就是等函数的resolve或者reject调用的时候（因为exector中的函数，必然会调用这两个，否则的话，Promise就一直是PENDING）
                this.succCallBacks.push(() => {
                    // console.log('succData',this.succData);
                    setTimeout(() => {
                        try {
                            let ret = onFulfill(this.succData);
                            analysePromise(p2, ret, resolve, reject);
                        } catch (e) {
                            reject(e);
                        }
                    }, 1000);
                });

                //注意这里写的不是this.succCallBacks.push(onFulfill(this.succData));而是包装了一层函数
                // 为什么？因为如果直接这样写，函数就执行了
                this.failCallBack.push(() => {
                    // console.log('failData',this.failData);
                    setTimeout(() => {
                        try {
                            let ret = onReject(this.failData);
                            analysePromise(p2, ret, resolve, reject);
                        } catch (e) {
                            reject(e);
                        }
                    }, 1000);
                });
            }
        });
        return p2;
    }
    // 实例方法catch,返回的还是Promise。当出现异常的时候会走
    // 要获取到调用者的状态
    catch(callback) {
        return this.then(null, callback);
    }

    /**
     * 返回一个成功的Promise
     * @param {返回的值} val 
     * @returns 
     */
    static resolve(val) {
        return new Promise((resolve, reject) => {
            resolve(val);
        })
    }

    /**
     * 返回一个失败的Promise
     * @param {失败值} val 
     * @returns 
     */
    static reject(val) {
        return new Promise((resolve, reject) => {
            reject(val);
        })
    }

    /**
     * finally方法，不论成功还是失败，都会走
     * 但是finally并不接受上一个promise的返回值，也不接受它的报错。
     * 直接将成功或者失败传递到下一个Promise
     */
    finally(callback) {
        let constructor = this.constructor;
        // 上一个Promise的成功还是失败，决定了下一个的成功失败。那我怎么知道成功还是失败？
        return this.then(
            val => constructor.resolve(callback()).then(() => val)
            , rson => constructor.resolve(callback()).then(() => { throw rson }));
        // return this.then((val)=>constructor.resolve(val),(rson)=>constructor.reject(rson));
    }


    /**
     * 所有的promise都结束后返回结果
     * 如果有reject的Promise，就直接走到catch里面
     * @param  promises promise实例
     * @returns 返回所有Promise实例的值的集合
     */
    static all(promises) {
        return new Promise((resolve, reject) => {
            // 保存promise的结果
            let result = [];
            let times = 0;
            // 定义函数
            let processData = (index, value) => {
                result[index] = value;
                times++;
                // 如果调用够了次数，就resolve
                // 体会一下为什么要用一个times来计数？而不是直接result.length == promises.length 因为如果碰到异步任务,就会出现某个值为空的情况
                if (times == promises.length) {
                    resolve(result);
                }
            }
            for (let index = 0; index < promises.length; index++) {
                const promise = promises[index];
                // 判断是不是一个promise
                if (isPromise(promise)) {
                    // 调用then方法，
                    promise.then((data) => {
                        processData(index, data);
                    }, (e)=>{
                        reject(e);
                    })
                } else {
                    // 普通值
                    processData(index, promise);
                }
            }
        })
    }
}
//导出模块---CommonJS 规范
module.exports = Promise;
