// 对webpack进行配置，因为基于Node，所以可以写node代码
// 模块化导出
const path = require('path');
module.exports = {
    // 项目打包的入口
    entry:'./src/index.js',
    output:{
        // 打包的输出目录
        path: path.resolve(__dirname,'dist'),
        // 打包后的入口文件
        filename:'bundle.js'
    }
}