// 在此导入当前目录下所有的组件

/**
 * 获取当前目录下所有的vue结尾的文件
 * webpack的API，
 * 第一个参数 路径
 * 第二个参数 是否进行递归
 * 第三个参数 文件格式
 */
const requireConponnents = require.context('./', false, /\.vue$/);

/**
 * 字符串首字母大写
 * @param {目标字符串} str 
 * @returns 
 */
function firstUpper(str) {
    if (!srt) return
    return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * 从文件名字中获取组件名字
 * 如 ’./hello.vue‘  获取到hello
 */
function getCompName(fileName) {
    return fileName.replace(/^\.\//, '').replace(/\.\w+$/, '');
}

/**
 * 遍历所有的文件，进行挂载
 */
requireConponnents.keys().forEach(fileName => {
    // 注册组件，一般是注册的 Vue.component('Hello',hello)的形式，所以首字母大写
    const config = requireConponnents(fileName);

    const compName = getCompName(fileName);
    // 注册组件
    Vue.component(compName, config.default || defualt);
});