const http = require('http');
const fs = require('fs').promises;
const path = require('path');
const url = require('url');
const mime = require('mime')//使用需要先安装？

class Server {
    constructor(config){
        // 构造函数中将配置传入，如果没传则使用默认值
        // process.cwd()代表的是当前的文件夹
        this.config = {port:3000,directory:process.cwd(),...config};
        console.log(this.config.port);
    }
    /**
     * 处理请求的核心逻辑
     * @param {request} req 
     * @param {response} res 
     */
    handleReq(req,res){
        /*
         * 请求的是文件夹就列出下面的文件，否则返回文件。 
        */
        let { pathname } = url.parse(req.url);
        // 如果输入的是中文，会导致乱码，所以需要解码
        pathname = decodeURIComponent(pathname);
        let absPath = path.join(process.cwd(),)
    }
    start(){
        let server = http.createServer(this.handleReq.bind(this));
        // 启动服务
        server.listen(this.config.port);
    }
}

module.exports = Server;