import { Message } from 'ant-design-vue';
const VCopy = {
    bind(el, { value }) {
        el.$value = value;
        el.handler = () => {
            if (!el.$value) {
                // 无内容复制则给提示
                Message('无复制内容');
                return;
            }

            const textArea = document.createElement('textarea');
            // 将texrarea 设置为readonly，防止弹起键盘。同时设置到屏幕外。
            textArea.readOnly = 'readonly';
            textArea.style.position = 'absolute';
            textArea.style.left = '-9999px';
            // 将复制的值设置到textarea
            textArea.value = el.$value;
            // 将元素添加到body
            document.body.appendChild(textArea);
            // 选中值并且复制
            textArea.select();
            // 或者
            // textArea.setSelectionRange(0,textArea.value.length);
            const result = document.execCommand('Copy');
            if (result) {
                Message.success('复制成功');
            }
            document.body.removeChild(textArea);
        }

        // 绑定事件
        el.addEventListener('click', el.handler);
    },
    // 当传进来的值更新的时候触发
    componentUpdated(el, { value }) {
        el.$value = value;
    },
    // 指令与元素解绑的时候执行
    unbind(el) {
        el.removeEventListener('click', el.handler);
    }
}
export default VCopy;