import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 是否展示右上角的导航
    showNav: false
  },
  mutations: {
    // 默认取反
    changeShowNav(state,payload){
      payload = payload === undefined ? !state.showNav : payload;
      state.showNav = payload;
    }
  },
  actions: {
  },
  modules: {
  }
})
