// 请补全JavaScript代码，要求实现对象参数的深拷贝并返回拷贝之后的新对象。
// 注意：
// 1. 参数对象和参数对象的每个数据项的数据类型范围仅在数组、普通对象（{}）、基本数据类型中]
// 2. 无需考虑循环引用问题

const _sampleDeepClone = target => {
    // 补全代码
    if (target === null || typeof target !== 'object') {
        // 基本数据类型
        return target;
    }
    let ret = Array.isArray(target) ? [] : {};
    Object.keys(target).forEach(key => {
        ret[key] = _sampleDeepClone(target[key])
    })
    return ret;
}

let obj = {
    a: 123,
    b: [1, 2, 3],
    c: {
        d: 1,
        e: [4, 6, 7]
    }
}
let clone = _sampleDeepClone(obj);
obj.b.push(5);
obj.c.e.push(678);
console.log(obj);
console.log(clone);