// 导入createApp函数
import { createApp } from 'vue'
// 导入App.vue
import App from './App.vue'
// 导入路由
import router from './router'
// 基于根组建创建Vue
const app = createApp(App);
// 挂在路由
app.use(router);
// 挂载到ID为APP的容器上
app.mount('#app')
