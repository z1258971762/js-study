// 定义install函数
export let _Vue;
import Link from './components/link';
import View from './components/view';
/**
 * 让所有的组件或者Vue实例都可以访问到router路由实例对象
 * @param {Vue实例} Vue 
 * @returns 
 */
export default function install(Vue) {
    // 安装过了，直接返回
    if (install.installed && _Vue === Vue) return;
    install.installed = true;

    _Vue = Vue;

    Vue.mixin({
        beforeCreate() {
            if (this.$options.router) {
                // 这里保存的其实是vue实例
                this._routerRoot = this;
                // 保存new出来的router对象
                this._router = this.$options.router;
                // 调用初始化方法
                this._router.init(this);
                // 在Vue实例上添加一个响应式属性，_route 用来表示当前的路由
                Vue.util.defineReactive(this, '_route', this._router.history.current);
            } else {
                // 这一步保证所有的组件都拿父亲身上的_routerRoot，也就是所有的组件都能访问到Vue根实例
                this._routerRoot = (this.$parent && this.$parent._routerRoot) || this;
            }

            // 因为每一个组件都能访问，this.$router(路由实例) 和 this.$route（当前路由） 所以需要定义在Vue的原型上
            // if(!(Vue.prototype.$router || Vue.prototype.$route)){
                
            //     console.log('tertretret ',this._routerRoot);
            // }
        }
    })

    // 注册组件
    Vue.component('RouterLink', Link);
    Vue.component('RouterView', View);

    // console.dir('this',this);
    Object.defineProperty(Vue.prototype, '$router',{
        get(){
            console.log('调用时的this',this);
            // 本来以为这里的this 会是Vuerouter实例，而取不到_routerRoot。但是其实这里的this并不是Vuerouter 而是Vue实例，因为这里的this是取决于调用方的
            return this._routerRoot._router;
        }
    });

    Object.defineProperty(Vue.prototype, '$route',{
        get(){
            return this._routerRoot._route;
        }
    });
}