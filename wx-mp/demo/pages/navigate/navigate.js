// pages/navigate/navigate.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  navigateTo(){
    // 跳转切换Tab
    // wx.switchTab({
    //   url: '/pages/localLife/localLife',
    // })
    // 跳转页面
    wx.navigateTo({
      url: '/pages/index/index',
    })
    // 返回跳转
    wx.navigateBack({
      // 返回两页
      // delta如果大于页面栈的最多，则返回第一个页面
      delta:2
    });
  },

  navigateToWithArgs(){
    wx.navigateTo({
      url: '/pages/logs/logs?a=10&b=20',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 页面加载完了就执行。在这里获取页面跳转的参数
    console.log(options);
    console.log(options.a);
    console.log(options.b);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 获取当前页面栈
    console.log(getCurrentPages());
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // 监听用户的下啦事件，可以在这里做一些事
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})