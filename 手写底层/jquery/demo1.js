// 自执行，防止变量污染，相当于单例模式
(function (window) {
    window.$ = jquery = function (selector) {
        let nodes = {};
        if (typeof selector == 'string') {
            let temp = document.querySelectorAll(selector);

            for (let i = 0; i < temp.length; i++) {
                nodes[i] = temp[i];
            }
            nodes.length = temp.length;
        } else {
            throw new Error('必须输入字符串')
        }
        /**
         * 给元素增加样式类
         * @param {类名的集合} classes 
         */
        nodes.addClass = function (classes) {
            let className = classes.split(' ');
            className.forEach(name => {
                for (let i = 0; i < nodes.length; i++) {
                    nodes[i].classList.add(name);
                }
            });
        }
        /**
         * 给元素设置文本信息
         * @param {文本信息} text 
         */
        nodes.setText = function (text) {
            for (let i = 0; i < nodes.length; i++) {
                nodes[i].textContent = text;
            }
        }
        return nodes;
    }
})(window)