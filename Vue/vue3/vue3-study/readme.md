## Vue3创建工程
+ `npm init vite-app project-name` 或者 `npm create vite@latest my-vue-app --template vue`
+ 当然，也可以先安装脚手架`vue-cli`然后再创建项目。
+ `cd project-name` 然后 `npm i` 
+ 启动项目`npm run dev`

## 优点
+ 性能提升
+ 体积小
+ TS支持
+ 组合API

## 组合API与选项API
+ 选项API代码比较分散
+ 组合API代码比较集中

## 组合API注意事项
+ 组合API 的起点是setup函数
+ 这个函数会在市里创建之前，也就是Vue2的`beforeCreate`之前去执行
+ setup 函数中因为在最前面执行，所以没有this，this指向的是undefined，不能通过this来获得实例
+ 模版中需要用到的数据以及方法，需要在setup函数中进行返回
+ 对象直接解构后返回是不具有响应式的，所以不能直接解构，要借助toRef函数来实现响应结构

## 组合API的生命周期
+ setup
+ onBeforeMount
+ onMounted
+ onBeforeUpdate
+ onUpdated
+ onBeforeUnmount  卸载前
+ onUnmounted  卸载后

## 组合API中的一些函数
+ reactive({})  让对象数据具有响应式
+ toRef(target,attr)   转换响应式对象中某个具体属性---参数必须是响应式对象,转换完毕为一个对象
+ toRefs({})  转换响应式对象的所有属性，接收的参数是响应式的对象
+ ref()   让普通值具有响应式---当数据未知的情况下可以ref(null)定义响应式