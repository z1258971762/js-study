# wy-music

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### 使用组件

- vant -- vant 组件
- postcss -- 将 PX 转为 rem 做移动端适配
- axios -- 发送请求
- npm i babel-plugin-import -D 自动引入 vant 的样式
- 如果没有 vue-router 也需要安装一下
- "postcss-pxtorem": "^5.1.1" 这个版本不能太高，否则识别不了
