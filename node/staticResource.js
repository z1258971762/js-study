const http = require('http');
const fs = require('fs');
const path = require('path');
const queryString = require('queryString');
const url = require('url');
// 处理内容类型，通过文件的扩展名来返回响应方式，text/html....
// npm i mime
const mime = require('mime');
console.log(mime.getType('mp4'));

// 解析请求，将请求的网页返回回去，并回显
let server = http.createServer((request, response) => {
    const { pathname, query } = url.parse(request.url, true);
    console.log(pathname);
    // 这里用的join而不是resolve。因为resolve遇到开头的/会当作根目录处理。
    const absPath = path.join(__dirname, pathname);
    // 文件类型
    const fileType = mime.getType(absPath);

    // 判断是文件还是目录，是目录则找index.html,否则直接找文件
    fs.stat(absPath, (err, statObj) => {
        if (err) {
            // fanhui 404
            if (err) {
                response.statusCode = 404;
                return response.end('NOT FOUND');
            }
        }
        // 如果是文件，直接读取
        if (statObj.isFile()) {
            readFile(absPath, response);
        } else {
            // 否则读取目录下的HTML
            readFile(path.join(absPath, 'index.html'), response);
        }
    })

});

/**
 * 定义一个读取文件的函数，找到文件就返回找不到就404
 * @param {文件路径} path 
 * @param {response} response 
 */
function readFile(path, response) {
    // 文件类型
    const fileType = mime.getType(path);

    fs.readFile(path, (err, data) => {
        if (err) {
            response.statusCode = 404;
            return response.end('NOT FOUND');
        }
        // 不设置的话，会乱码。如果是text/plain浏览器直接展示，不会解析
        response.setHeader('Content-type', fileType + ';charset=utf-8');
        response.end(data);
    })
}

server.listen(3000, () => {
    console.log('server start at port 3000');
});