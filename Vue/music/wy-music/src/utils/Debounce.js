/**
 * 防抖函数
 * 如果在函数中使用了this，那么记住一定要传入function形式的函数，否则this会丢失的
 * @param {目标函数} fn 
 * @param {延迟时间} delay 
 * @returns 返回目标函数
 */
const Debounce = (fn, delay = 500) => {
    let timer;
    return function () {
        if (timer) {
            clearTimeout(timer);
        }

        timer = setTimeout(() => {
            fn.call(this, ...arguments)
        }, delay);
    }
}


/**
 * 节流函数
 * @param {函数} fn 
 * @param {延迟，单位毫秒} delay 
 */
const Throttle = (fn,delay = 1000) =>{
    let timer;
    return function(){
        if(timer){
            return;
        }
        timer = setTimeout(() => {
            fn.call(this,...arguments)
            timer = null;
        }, delay);
    }
}

export {
    Debounce,
    Throttle
}