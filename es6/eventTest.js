const EventEmitter = require('./MyEvent');
const util = require('util');

let event = new EventEmitter();

let sing = () => { console.log('sing') };
let dance = () => { console.log('dance') };
let paint = () => { console.log('paint') };

//订阅事件
event.on('happy', sing)
event.on('happy', dance)
event.on('happy', paint)
//发布事件
event.emit('happy');
console.log('------------------------');
//移除事件
event.off('happy', paint);
event.emit('happy');

console.log('-------------事件监听-----------');
//事件监听
event.on('newListener', () => { console.log('lister1'); })
//可以在这里直接触发事件process.nextTick把发布做成异步的，这样的话可以将本次订阅触发
event.on('newListener', () => { process.nextTick(() => { event.emit('happy') }) });

event.on('happy', () => { console.log('监听进来的1'); })

console.log('-------------继承方式-----------');
function Boy() { }
//实现继承
util.inherits(Boy, EventEmitter);

let boy = new Boy();

boy.on('下班', () => { console.log('games'); });
boy.emit('下班');