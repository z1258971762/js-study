import Vue from 'vue'
// import Vuex from 'vuex'
// 引入自己的Vuex
import Vuex from './MyVuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    age: 18
  },
  // 计算属性，相当于是computed
  getters: {
    newAge: (state) => {
      return state.age + 1;
    }
  },
  mutations: {
    add(state, payload) {
      state.age += payload;
    },
    sub(state, payload) {
      state.age -= payload;
    }
  },
  actions: {
    // asyncSub(store,payload){
    //   setTimeout(()=>{
    //     store.commit('sub',payload);
    //   },2000)
    // },
    // 解构的形式，会使this问题暴露，可能会导致this丢失，所以actions和mutations的赋值方式很重要
    asyncSub({commit},payload){
      setTimeout(()=>{
        commit('sub',payload);
      },2000)
    }
  },
  modules: {
  }
})
