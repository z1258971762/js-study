import axios from "../http";

// 获取歌曲的歌词信息
const getLyricById = id => axios.get(`/lyric?id=${id}`)
// 获取歌曲详细信息
const getSongDetail = ids => axios.get('/song/detail',{params:{ids}})
// 获取歌曲的流信息---header信息和responseType要加上，否则下载下来不能播放
const getSongStream = url => axios.get(url,{responseType: 'blob',headers: {'content-type': 'audio/mpeg'}});

export {
    getLyricById,
    getSongDetail,
    getSongStream
}