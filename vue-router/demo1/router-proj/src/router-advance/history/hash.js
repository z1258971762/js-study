import History from "./base";

/**
 * HASH 模式，URL会有# 
 */
export default class HashHistory extends History{
    constructor(router){
        super(router);
        ensureSlash();
    }

    /**
     * 获取当前的路由地址
     */
    getCurrentLocation(){
        return window.location.hash.slice(1);
    }
    /**
     * 监听路由地址变化
     */
    setUpListener(){
        window.addEventListener('hashchange',()=>{
            console.log('增加监听',this);
            this.transtionTo(this.getCurrentLocation());
        })
    }
}

/**
 * 确保首次访问地址加上 #
 */
function ensureSlash(){
    // 如果有hash。就返回
    if(window.location.hash){
        return
    }
    // 否则加上hash--这里不必自己加# ，浏览器会自动加
    window.location.hash = '/';
}