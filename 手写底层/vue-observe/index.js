/**
 * 依赖收集 + 发布订阅
 * 要知道监听哪个对象的什么属性，
 * 要知道通知的DOM元素是什么
 * 要给 每个元素一个唯一的ID去记住订阅的事件
 */
let Dept = {

    orderMap: {},

    listen(key, callback) {
        (this.orderMap[key] || (this.orderMap[key] = [])).push(callback);
    },
    /**
     * 通知执行回调
     * @returns 
     */
    notify() {
        let keys = Array.prototype.shift.call(arguments),
            fns = this.orderMap[keys];

        if (!fns || fns.length == 0) return

        for (let i = 0,fn;fn = fns[i++];) {
            fn.apply(this, arguments);
        }
    }
}
/**
 * 将对象变为响应式的
 */
function ObserveAble(object, objKey, uniqueKey, selector) {
    let value = '';
    let dom = document.querySelector(selector);
    Object.defineProperty(object, objKey, {
        get() {
            console.log(`调用了get ${objKey}`);
            return value;
        },
        set(val) {
            console.log(`调用了set ${objKey}`);
            value = val;
            Dept.notify(uniqueKey,value)
        }
    })
    Dept.listen(uniqueKey, function (text) {
        dom.innerText = text;
    });
}