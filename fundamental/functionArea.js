// 作用域和原型
function Person(){
    this.nation = 'china';
}

Person.prototype.getNation = function(){
    console.log(this.nation);
}

// 继承Person
function Student(name){
    Person.call(this);
    this.name = name;
}

// 继承原型方法
let obj = Object.create(Person.prototype);
Student.prototype = obj;
Student.prototype.constructor = Student;

let per = new Person();
console.log('self',per.nation);
per.getNation();


let  stu = new Student('xiaoming');
console.log('name',stu.name);
console.log('nation',stu.nation);
console.log(stu);
console.log(Student.prototype);
stu.getNation();



// 现在开始尝试将作用域和原型结合起来
(function fun1(){
    console.log('-----------------------');
    let st1 = new Student('zhangsan');
    function fun2() {
        // 首先当前作用域找不到st1，所以去父级作用域去找。这个是作用域链
        // 恰好父级作用域中有st1.
        // 而st1上自己没有nation属性，也没有nation方法，于是沿着原型链去查找。
        // 而原型链中也恰好具有这个属性。
        // 所以作用域链和原型链是两个东西
        console.log(st1.nation);
        st1.getNation();
    }
    fun2();
})();