## 什么是Vue

渐进式的JS框架（最开始只是一个库、只是一个工具，后来变成框架，有自己的实现逻辑）
框架对项目侵入比较高，但是库侵入比较少。就是自己的核心能够独立工作，也能配合其他东西整合在一起使用

**全家桶：**（Vue Core + Components + vue-router + vuex+ webpack）
***多看官方的文档***


## Vue的特点
+ 开发者不需要或者减少了操作DOM，vue底层帮我们操作
+ 数据驱动视图，数据改变的时候，绑定数据的视图也会随之更新

### 数据驱动

数据改变之后，驱动视图改变。而且这些改变，不需要我们开发者去改变，不需要手动操作DOM

### MVVM

Model 数据部分、View视图部分、VM桥梁部分（数据和DOM元素之间的桥梁）Vue实例就是这个角色


## Vue的基本语法
Vue 的代码最终都会被编译成为原生JS

### 视图层View 
```html
    <div id="app">
        {{content}}
    </div>
```

### 视图模型层 VM
```javascript
    const vm = new Vue({
        el:'#app',//CSS 选择器，选择Dom元素
        // 这里的对象可以是对象，也可以是函数，但是在组件中，这里必须是函数
        // Mode 层
        data:{
            // data上的元素直接挂在了vm上了，数据改变，视图直接更新
            content:'from Vue data'
        }
    })
```
+ **data：** 选项是定义数据的，可以是对象或者是函数，在组件中，必须是函数
+ **MVVM：** 指的就是Module、View、VM（Vue实例）
+ **el：** el的值是css selector /选择dom元素
+ **{{}}:** 插值表达式，把数据填充到HTML标签中，可以做一些基础的表达式运算

## 模板语法
    模板 + 数据 -》 静态的html内容

如何进行前端数据渲染（把数据填充到HTML）？
+ 字符串拼接
+ 模板引擎（art-template）
+ vue （高级模板引擎）

### 需要掌握的
+ 插值表达式（双大括号的语法）
+ 指令（自定义属性，v-开头的）
+ 事件绑定
+ 属性绑定
+ 样式绑定
+ 逻辑判断、循环

## 常见指令

+ v-cloak（一般用来控制页面元素的加载，解决页面闪烁问题）

+ v-html / v-text

  + v-html指的是，将内容填充到目标标签，可以解析HTML，相当于innerHtml（可能有安全问题）
  + v-text指的是，将文本内容填充到标签，但是不支持HTML，相当于innerText
  + `<div v-text="这是一个text"></div>`

+ v-show。 条件显示。不管显示不显示，都会渲染。

+ v-if  条件渲染（比较简单）可以配合v-else 使用

+ v-for。循环渲染，可以循环列表，也可以循环对象的key`v-for ="(val,key,index) in object"`

+ v-on 绑定事件

  +  v-on:click = "a++";  =======或者 @click = "a++";

  + 如果想要获取事件对象的时候怎么办？如果函数**没有传参**，则函数默认会传递事件对象。如果自己传了参数，那么，需要人为手动的进行事件对象的参数传递，如`@click="add(5,$event)"` 通过此方法将事件对象**$event**进行传递。并在函数中接收即可。

  + 事件对象的target 是真正触发函数的元素

  + `@click.prevent.stop = "handle"` **prevent**阻止默认行为(preventDefault)，比如超链接不进行跳转，**stop**阻止冒泡（stopPropgation）

  + `@keyup.enter="handle"` 按键修饰符，按回车时触发 

  + 自定义按键修饰符：

    ```html
    <div id="app">
      <input type="text" @keyup.f1="handle" />
    </div>
    <script>
      // 尽管是自定义，但是必须是event中keyCode的值，实际上监听的还是值
      Vue.config.keyCodes.f1 = 112;
      let vm = new Vue({
        el:'#app',
        data:{
          
        },
        methods:{
          handle(){
            console.log('自定义修饰符')
          }
        }
      })
    </script>
    ```

+ v-bind  动态绑定到data中的一个值上，简写是：`:href="url"` = `v-bind:href = "url"`。

  + 什么时候**加冒号**，什么时候不加？如果这个值**不需要动态绑定，所写即为值，那就不需要加冒号**。
  + **v-model 是双向绑定，即页面上值改，数据也改。但是bind是单向的，只有数据改变了，视图才更新，所以页面上的值改了，bind绑定的数据是不会更新的**
  + 既想传递参数，又想获取事件对象，那么在最后传递一个参数（$event）就好。一定要放最后

+ v-model。一般用在表单上，双向数据绑定（操作视图，渲染视图也跟着变）

  + V-model.number = "num" 在获取num的时候，会自动被转换成number的类型
  + v-model.trim = 'str' 获取str时去掉首尾的空格
  + v-model.lazy  当内容改变并且按回车的时候才会改变。相当于change事件

+ v-pre  把内容原封不动的输出，不会当作模板语法来解析

+ v-once 表示只绑定一次，后面数据改了，页面不会更新（一般是针对后面不需要修改的数据）

### 动态绑定样式

```html
<style>
  .current{
    width: 200px;
    height: 200px;
    border: 1px solid darkgray;
  }
  .error{
    background-color: pink;
  }
</style>
<div id="app">
  <!-- 满足条件才加current -->
  <div :class="{current:isCurrent,error:isError}">123</div>
  <!-- 数组的形式 -->
  <div :class="['current','error']">345</div>
  <!-- 混用 -->
  <div :class="[{current:isCurrent},'error']">567</div>
</div>

<script>
  const vm = new Vue({
    el:'#app',//CSS 选择器，选择Dom元素
    // 这里的对象可以是对象，也可以是函数，但是在组件中，这里必须是函数
    data:{
      // data上的元素直接挂在了vm上了，数据改变，视图直接更新
      content:'from Vue data',
      isCurrent:true,
      isError:true,
    }
  })
</script>
```

### 自定义指令

```html
<!-- 自定义指令使用 -->
<div>
  <input type="text" v-focus value="自动聚焦" v-color= "{color:'red'}">
  <!-- input 上 增加autoFocus属性，可以自动聚焦，但是有兼容性问题 -->
</div>

<script>
  // 全局自定义指令
  Vue.directive('focus',{
    // 不同的钩子函数 inserted、bind、update
    // 当元素被插入到DOM树上的时候，自动聚焦
    inserted: function(el,binding,){
      //聚焦元素
      el.focus()
    },
    // 只调用一次，指令第一次绑定到元素的时候调用，在这里可以做一些初始化设置
    bind:function(){

    },
    // 所在组件的VNode进行更新的时候调用。
    update(){

    },
    // 所在组件VNode及所有子VNode全部更新之后调用
    componentUpdated:function(){

    },
    // 只调用一次，解绑的时候调用
    unbind:function(){

    }
  });

  Vue.directive('color',{
    bind:function(el,binding){
      console.log('binding---',binding);
      el.style.background = binding.value.color
    }
  })
  const vm = new Vue({
    el:'#app',//CSS 选择器，选择Dom元素
    // 这里的对象可以是对象，也可以是函数，但是在组件中，这里必须是函数
    data:{
      // data上的元素直接挂在了vm上了，数据改变，视图直接更新
      content:'from Vue data',
      isCurrent:true,
      isError:true,
    },
    // 局部自定义指令
    directives:{
			focus:{
        inserted:function(el){
          el.focus();
        }
      }
    }
  })
</script>
```

自定义指令中，可以接收很多的参数，可以看看官方文档

## 生命周期

+ beforeCreate
+ created
  + 此阶段之后数据才具有响应式
  + data、computed、watch中的数据都能用了
  + 既然data中的数据可以拿到，所以此阶段可以发一些异步请求，获取后端的数据
  + 但是此阶段不能拿到`$el`属性
+ beforeMount
  + 此阶段虚拟DOM已经有了，创建完成。但是还没有渲染
  + 此阶段可以进行数据的更改，并且**不会触发**update的生命周期
  + 此阶段拿到的$el还是原样子，没有经过Vue的处理。
+ mounted
  + 此阶段的$el拿到的就是Vue处理过后的数据了。所有的差值表达式已经被真实的数据所替换
  + 数据和真实DOM都创建完毕了。也就是说真实DOM加载完毕了。
+ beforeUpdate
+ updated
+ beforeDestroy
  + 此阶段之后watch、子组件，事件监听销毁
  + 此阶段一般做一些清理的工作
+ destoryed

## 计算属性/侦听器

```html
<div id="app">
  <!-- 此时调用一次，执行一次。方法不存在缓存 -->
  {{Upper()}}
  <!-- 计算属性，带有缓存,不需要加括号 -->
  {{Upper2}}
  <input type="text" v-model = "num1">
  <input type="text" v-model = "num2">{{num3}}
</div>

<script>
  new Vue({
    el:'#app',
    data:{
      msg:'vue computed',
      num1:0,
      num2:0,
      num3:0,
      obj:{
        abc:100
      }
    },
    methods:{
      Upper(){
        return this.msg.toUpperCase();
      }
    },
    // 计算属性本质上也是一个函数，使用的时候当作属性来用
    // 他是基于他的依赖来急性缓存的
    computed:{
      Upper2(){
        return this.msg.toUpperCase();
      }
    },
    // 监听属性
    watch:{
      // 这里放的是监听的属性，名字不能随便起
      // 只要数据变更了，就会执行指定的代码段
      num1:function(newV,oldV){
        console.log(newV,oldV);
        this.num3 = this.num1 + this.num2;
      },
      // 上下两种书写方式都可以，直接写函数，是省略了handler
      obj:function(){
        console.log('OBJ 改变了，但是监听不到里面abc的变化');
      },
      obj:{
        handler:function(){
          console.log('obj变了，并且能检测到abc的变化');
        },
        // 深度监听属性
        deep:true
      }
    }
    // watch 与computed的区别
    // watch可以将依赖分开来监听
    // 
  })
</script>
```

### watch 与computed的区别

**watch可以将依赖分开来监听**

**watch可以放一些异步的任务，或者开销比较大的操作**

 Watch 可以深度监听

## 过滤器

对数据进行一些处理，或者格式化。本质上就是一个函数

过滤器可以用在两个地方，一个是在插值表达式中，还有就是可以用在v-bind中

`<div :info="infos | upper"></div>`

```html
<body>
    <div id="app">
        <!-- 过滤器的使用 -->
        {{'hello' | upper | lower}}
        {{'hi' | upper2(10)}}
    </div>
    <script>
        // 全局过滤器
        Vue.filter('upper',function(val){
            return val.charAt(0).toUpperCase() + val.slice(1);
        })
        Vue.filter('lower',function(val){
            return val.charAt(0).toLowerCase() + val.slice(1);
        })
        new Vue({
            el:'#app',
            data:{
                msg:'vue computed',
            },
            methods:{
                
            },
            // 局部的过滤器(第一个参数是左边的值，第二个是传递的参数)
            filters:{
                upper2(val,args){
                    return val.charAt(0).toUpperCase() + val.slice(1) + args;
                }
            }
        })
    </script>
</body>
```



## 组件

组件之间通信存在三种情况，分别是：父传子，子传父，兄弟之间通信

```html
<div id="app">
    <div :style="{color}">{{msg2}}</div>
    <div :style="{color:rgbcolor}">字体颜色改变</div>
    data 中定义的是rgbcolor:'10,234,78'
    <pre>-----------------------子组件--------------------</pre>
    <menu-item :msgf="msg1" stat="static" ana-msg="驼峰" @change-color="changeColor"></menu-item>
</div>

<script>
    // 全局注册组件
    Vue.component('menu-item', {
        data() {
            return {

            }
        },
        template: `<div>
        <div>{{msgf}}</div>
        <div>{{stat}}</div>
        <div>{{anaMsg}}</div>
        <button @click="changeFatherColor">改变父组件的color</button>
            </div>`,
        props: [
            'msgf', 'stat', 'anaMsg'
        ],
        methods: {
            changeFatherColor() {
                // 其实就是一个发布订阅
                this.$emit('change-color', 'green')
            }
        }
    })
    // 兄弟通信测试
    // 兄弟之间通信使用eventBus。实际上就是发布订阅,只不过Vue身上有这两个方法，所以可以直接使用
    // 实际上和子传父差不多。
    // 移除的时候用 eventBus.$off('comp2-event');
    const eventBus = new Vue();
    Vue.component('comp1', {
        data() {
            return {
                num: 0,
            }
        },
        template: `
        <div>
        <p>comp1的值：{{num}}</p>
        <button @click="handle">改变comp2的值</button>
            </div>
        `,
        methods: {
            handle() {
                this.$emit('comp2-event', 2);
            }
        },
        mounted() {
            eventBus.$on('comp1-event', (v) => {
                this.num += v;
            })
        }
    });
    Vue.component('comp2', {
        data() {
            return {
                num: 0,
            }
        },
        template: `
                <div>
                <p>comp2的值：{{num}}</p>
                <button @click="handle2">改变comp1的值</button>
                    </div>
                `,
        methods: {
            handle2() {
                this.$emit('comp1-event', 3);
            }
        },
        mounted() {
            eventBus.$on('comp2-event', (v) => {
                this.num += v;
            })
        }
    })

    //根实例是父组件，menu-item是子组件
    const vm = new Vue({
        el: '#app',
        data() {
            return {
                msg1: 'foo',
                msg2: 'content',
                color: 'red',
                rgbcolor: 'rgb(10,200,65)'
            }
        },
        methods: {
            changeColor(color) {
                this.color = color;
            }
        }
    })
</script>
```

**注意⚠️：** 组件在使用的时候是不区分大小写的。所以驼峰定义的组件，使用的时候要使用短横线。要么就是定义的时候就用短横线去定义。

+ 父组件向子组件传值的时候，如果父组件中的属性，是短横线命名，那么子组件需要使用驼峰式接受
+ 父组件使用驼峰命名，子组件需要使用全部小写的形式
+ 最好是都使用小写的形式，两边都小写就好
+ 上面说的情况，都是适用于在DOM结构中的情况。
+ 如果父组件是模板字符串的形式定义并引用子组件，那么，**两边的变量定义可以统一形式**

**组件的另一种写法：**

```html
//这个比较舒服，写在HTML中，有提示
<script type="text/x-template" id = "counterTemp" >
  	<div>
  		<h1> 我是组件内容</h1>
  	</div>
</script>
<script>
  // 使用选择器的方式
	Vue.template('counter',{
    template:"#counterTemp"
  })
</script>
```

**标签缓存**

使用keep-alive来缓存，可以保持先前的操作状态，避免重新渲染，导致性能问题

```vue
<keep-alive>
	<component :is="compName"></component>
</keep-alive>
```

#### Props 验证

如果需要对父组件传来的数据进行校验，就需要使用props的校验

+ type:[String,Number]  或者 String
+ default: function(){ return XXX } 。 如果是基本值，那么可以直接写 default：10
+ validator:function（val）{ return true/false};



#### 组件通信

通信不仅仅可以通过props的方式进行传递，也可以通过其他方式操作数据（下面的方式可以跨组件进行通信，各有各的好处）

+ this.$root 代表的是根实例。相当于VM，可以直接操作或者读取上面的数据
+ this.$parent  代表的是父组件。
+ this.$refs.refName.xxx即可访问子组件中的数据。父组件在使用子组件的时候，要在子组件身上加一个ref属性，值与**refName**一致即可。
+ eventBus的方式进行通信，有一个缺点就是，**数据不支持响应式**。指的是在通信的时候，我把值传过去以后，这个传递的值如果变化了，那么不会更新到接受方
+ provide  inject 的方式，这个也是不支持响应式。但是在所有的后代中都可以使用inject，也就是可以跨级
+ $attrs/$listeners
+ vuex 适合大中型项目，数据状态比较复杂的时候

### 插槽

插槽，提升组件的复用能力。组件嵌套的内容，会替换插槽。插槽内可以写HTML代码

插槽的v-slot:header 可以被简写为#header

作用域插槽-->父组件操作子组件插槽中的数据进行处理

```html
<pre>-----------------------插槽1--------------------</pre>
<slot1></slot1>
<slot1>我不是默认的了</slot1>
<pre>-----------------------插槽2--------------------</pre>
<slot2>
    <template v-slot:header>
        我是插槽的头部
        <p>我也是插槽的头部</p>
        <div v-pre>
            v-slot一定要用template包起来，否则会报错 
            因为，v-slot一定要用在template上或者组件中
        </div>
    </template>

    <template v-slot:default>
        这个我没有指定名字，所以直接映射到默认的位置。
    </template>

    <template v-slot:footer>
        <div>这个是具名插槽的尾部</div>
    </template>
</slot2>

// 组件插槽--slot
Vue.component('slot1', {
template: `
<!--默认插槽-->
<div>
    <slot>我是插槽的默认内容</slot>
</div>
`
});

Vue.component('slot2', {
template: `
<!-- 具名插槽 -->
<div>
    <slot name="header"></slot>
    <slot></slot>
    <slot name="footer"></slot>
</div>
`
})
```

### 发送请求

ajax、fetch、axios看看官方文档。东西不多

HTML中的标签component相当于占位符，用来放置vue组件。通过is属性来指定标签的名字

`<component is="compName" ></component>`

## 路由（Vue Router）

引入了路由组件之后，在HTML中就可以是用router-view标签来占位了，用来替换路由的渲染。使用router-link来进行页面的跳转通过to属性来指定跳转的hash

路由的模式有**hash**模式和**history**模式。

路由匹配规则中一般写一个*的匹配，进行打底。一般是所有的都匹配不到的时候，才匹配。有匹配的时候，优先显示匹配的路由。

### 动态路由

动态路由一般是在路径后面跟上参数，比如：`http://localhost:3000/home/123`。此时在对应的路由中，可以通过`$route.params`来获得链接中携带的参数。当让这一切要基于路由匹配规则的匹配。此时，路由的匹配规则要写为：`path:/home/:userId` 。获取的时候，就是`this.$route.params.userId`

如果同一个路径可以匹配多个路由，那么匹配的优先级按照定义的顺序，先定义的优先级高

动态路由支持*进行匹配。如：`path:/user-*` 匹配的是以**user-**开头的路径

### 嵌套路由

嵌套路由即在某个路由页面，提供了跳转其他路由的地方。

指定子路由的路径，一般通过在父路由中，通过属性**children**来指定子路由的路径

```javascript
const router = new VueRouter({
            routes: [
                {
                    path: '/user',
                    component: User,
                    children:[
                        {
                            path:'info',
                            template:`
                                <div>
                                    <h3>这里是子路由</h3>
                                    <h4>子路由中的path，可以写相对路径，也可以写绝对路径。</h4>
                                    <h4>相对路径就是现在这样，绝对路径，就要以斜杠/开头，但是绝对路径要包含父路径</h4>
                                </div>
                            `
                        }
                    ]
                },
                {
                    path: '/goods',
                    component: Goods
                },
                {
                    path: '/rights',
                    component: Rights
                },
                {
                    path: '/',
                    redirect: '/user'
                },
                {
                    // 所有的都匹配不到的时候，走这个。优先显示对应的路径
                    path:'*',
                    component : {
                        template:`<h1>404 Not Found</h1>`
                    }
                }
            ]
        })
        const vm = new Vue({
            el: '#app',
            router
        })
```

#### 路由组件传参

路由组件传参的时候，在path的同级指定一个props属性。

props有三种形态，一种是boolean值。一种是对象，还有一种是函数。

boolean的情况时，在目标组件的props属性中，可以直接接收路径中的参数。

如果是对象的形式，那么在目标对象的props属性中，可以直接接收传递过来的对象。但是路径中的参数，就不能在props中接收到了。

如果路由中的props是函数，那么函数可以接收一个参数，可以在函数中返回一个对象。此时可以直接传递一些参数，同时还能将路径中的参数，通过函数中的形参.params.参数来获取。此时传递的参数，在目标组件都能通过props获取到。其实形参，就相当于是**$router**

```json
// path 的定义方式
{
  path: '/rights:id',
  component: Rights,
  //第一种
  props:true,
  //第二种
  props:
  {
  	para:'123'
	},
	props: param =>{ id:param.params.id,psd:'1234'}
}

// 目标组件的props
props:['id','psd','para']
```

### 路由模式

路由有hash模式，和history模式

hash模式带有#号，不好看

history模式需要服务器的支持，服务器需要做配置，当请求的不是首页的时候，就返回首页。这个可以装一个模块：`npm i connect-history-api-fallback` 使用的时候就express的 `app.use(history())`就可以了。但是位置有将就，放在最上面就好。只要是用来支持history模式。

为什么需要服务器支持？因为单页面应用只有一个页面。每当跳转的时候变得是组件，以及地址，那么每当刷新的时候，就会发送请求，服务器就会把请求当作资源来处理，那么就会出现找不到的情况。但其实这个是在首页的路由配置中配置的，所以访问其他页面的时候，就把首页返回给客户端即可

#### 命名路由

可以给路由起一个名字name，以此来区分页面，那么在跳转的时候，就可以通过name来进行跳转了

```html
<!--进行跳转,要加冒号，因为是个表达式-->
<router-view :to="{name:'home',param:{id:123}}"></router-view>
```

#### 编程式导航

```vue
// 进行跳转
this.$router.push({name:'组件名字',params:{a:1}})
this.$router.push({path:'路径名字',params:{a:1}})
// 这种方式带的参数，是会发送到链接上
this.$router.push({path:'路径名字',query:{a:1}})
this.$router.push('path')

// 取参数。一个是router，一个是route
this.$route.params.a
```

#### 命名视图

 ### 路由守卫

```javascript
router.beforeEach((to,from,next)=>{
  // to: 目标页面。   from 跳转来源    next 是否放行跳转。如果是next（false）就禁止放行，不会跳转
})
```

## Vue-cli

### 安装Vue-cli

Vue-cli是一个脚手架，全局安装Vue-cli。`npm i -g @vue/cli`

使用Vue-cli将工程模块化处理

### 初始化工程

+ `vue create my-vuex` 创建一个vue的工程
+ Manually select features. 可以选择自定义选择工具

### 项目目录&工程启动

启动工程：`cd my-vuex ----> npm run serve`

## VueX

Vuex是Vue官方提供的为Vue组件管理状态的一个插件，可以进行数据变更的监控

### 涨知识

+ Vue.use(Vuex);当执行这个方法的时候，会执行一个install方法。所以如果自定义一个工具或者什么，当使用Vue.use方法的时候，要注意提供一个install方法

+ 在执行install的时候，Vue会默认给我们传递一个参数，就是Vue实例

+ 因为在访问Vuex中的数据时，会使用`$store.state.XXX`的形式。所以需要把这个东西放到Vue的实例，以及他的子孙后代上。那么这歌操作就可以在**install**的方法中执行，并且需要使用一个东西，叫做**`mixin()`**

+ mixin 方法在官方教程中的混入一栏。**mixin混入**可以进行全局注册，一旦使用全局混入，那么将会影响每一个之后创建的Vue实例。所以使用的时候要小心

  ```javascript
  Vue.mixin({
    created:()=>{
      console.log('minxin created');
      var myOption = this.$option.myOption;
      if(myOption){
        console.log(myOption)
      }
    }
  })
  
  new Vue({
    myOption:'hello'
  })
  // ===> hello
  
  //上面使用mixin在组件的created声明周期指定创建的时候执行一段逻辑，那么每次新创建组件的时候，都会执行
  ```

+ 
