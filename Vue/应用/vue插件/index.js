// 插件的最外层index是入口文件。
// 用来统一管理组件。

// webpack的API。用来动态引入文件（引入路径，是否引入子文件夹，引入规则）
const requireComponent = require.context('./', true, /\.vue$/);

const install = (Vue) => {
    if (install.installed) return;
    // 标记一下已经安装了
    install.installed = true;

    // requireComponent.keys() => []  查找到的所有文件
    requireComponent.keys().forEach(fileName => {
        // 获得当前的文件
        const config = requireComponent(fileName);
        // 获取组件的名字，就是在default里面定义的名字
        const compName = config.default.name;
        //  全局注册组件 ---config.default 表示的是文件中，export default 中的部分
        Vue.component(compName, config.default || config);
    })

    // 添加一个自定义的指令
    Vue.directive('focus', {
        inserted(el) {
            el.focus();
        }
    })
}

// 环境检测/
if (typeof Window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

export default {
    install
}

// // 使用插件：在工程的main.js中，使用 Vue.ues(VueMsg); 来全局注册插件，