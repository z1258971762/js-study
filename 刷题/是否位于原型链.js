// 请补全JavaScript代码，要求以Boolean的形式返回第一个实例参数是否在第二个函数参数的原型链上。
// 但是这个实现，怎么看都像是，第二个对象是否出现在第一个对象的原型链上
const _instanceof = (target, Fn) => {
    // 补全代码
    // 获取原型对象
    return target instanceof Fn.prototype;
}

const _instanceof = (target, Fn) => {
    // 补全代码
    return Fn.prototype.isPrototypeOf(target);
}

const _instanceof = (target, Fn) => {
    // 补全代码
    // 如果为空，或者传入的不是对象，那就返回false
    if (target == null || target == undefined || typeof target != 'object') {
        return false;
    }
    let prototype = Fn.prototype;
    let prot = Object.getPrototypeOf(target);
    while (true) {
        if (prot == null) {
            return false;
        }
        if (prot === prototype) {
            return true;
        }
        prot = Object.getPrototypeOf(prot);
    }
}

function fn() { const Fn = function () { }; const o = new Fn(); const result = _instanceof(o, Fn) && !_instanceof(o, Array); return result }
console.log(fn());