export default{
    render(h){
        // 获取当前路由匹配的地址
        const route = this.$route;
        // 标记有roterView
        this.roterView = true;
        let depth = 0;
        let parent = this.$parent;

        while (parent){
            // 如果父亲也有routerVieW 那么就是在渲染子组件
            if(parent.roterView){
                depth ++;
            }
            parent = parent.$parent;
        }
        const matched = route.matched[depth];

        if(matched){
            return h(matched.component)    
        }
        return h();
    }
}