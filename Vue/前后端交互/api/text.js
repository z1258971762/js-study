let CryptoJS = require('crypto-js');
let lat = '8b46fd46a2a74afe'
let lng = '2f97690c5a7ded300887e626dbdc0478'
const LOCATIONKEY = 'xwtec&qp';
const ivHex = '12345678';
// const ivHex = [ 1, 2, 3, 4, 5, 6, 7, 8 ]
// function decryptByDES(ciphertext, key = LOCATIONKEY) {
//   //把私钥转换成16进制的字符串
//     var keyHex = CryptoJS.enc.Utf8.parse(key);
//   //把需要解密的数据从16进制字符串转换成字符byte数组
//   var decrypted = CryptoJS.DES.decrypt(
//     {
//       ciphertext: CryptoJS.enc.Hex.parse(ciphertext),
//     },
//     keyHex,
//     {
//       iv:CryptoJS.enc.Utf8.parse(ivHex),
//       mode: CryptoJS.mode.CBC,
//       padding: CryptoJS.pad.Pkcs7,
//     },
//   );
//   //以utf-8的形式输出解密过后内容
//   var result_value = decrypted.toString(CryptoJS.enc.Utf8);
//   console.log(result_value, 'result-value');
//   return result_value;
// }

// decryptByDES(lat)



// DES解密
const decryptByDES2 = function (ciphertext, key, iv1) {
    const keyHex = CryptoJS.enc.Utf8.parse(key)
    const iv = CryptoJS.enc.Utf8.parse(iv1)
    const decrypted = CryptoJS.DES.decrypt({
        ciphertext: CryptoJS.enc.Hex.parse(ciphertext)
    }, keyHex, {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    })
    return decrypted.toString(CryptoJS.enc.Utf8)
}

console.log('123',decryptByDES2(lng,LOCATIONKEY,ivHex));