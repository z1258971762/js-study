// 请补全JavaScript代码，要求实现Array.filter函数的功能且该新函数命名为"_filter"。
Array.prototype._filter = function (callback) {
    if (typeof callback != 'function') {
        throw new Error(`${callback} is not a function`);
    }
    let retArr = [];
    for (let i = 0; i < this.length; i++) {
        // 不考虑this的情况
        // if (callback(this[i], i, this)) {
        //     retArr.push(this[i]);
        // }
        // 考虑this的情况
        let ret = callback.call(arguments[1],this[i],i,this);
        ret && retArr.push(this[i])
    }
    return retArr;
}
console.log([1, 2, 3, 4]._filter(i => i < 3));