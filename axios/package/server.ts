// 开始封装Axios

import Request from "./index";
import type { RequestConfig } from './types';

const request = new Request({
    baseURL: import.meta.env.BASE_URL,
    timeout: 1000 * 60 * 5,
    interceptiors: {
        // 请求拦截器
        requestInterceptors: config => {
            console.log('实例请求拦截器');
            return config;
        },
        responseInterceptors: res => {
            console.log('实例响应拦截器');
            return res;
        }
    }
})

/**
 * 请求配置的数据结构
 */
interface CustomRequestConfig<T> extends RequestConfig {
    data: T
}
/**
 * 响应的数据结构
 */
interface CustomResponse<T> {
    responseCode: number
    responseMessage: string
    success: boolean
    result: T
}

// 请求方法
const customRequest = <D, T = any>(config: CustomRequestConfig<D>) => {
    const { method = 'GET' } = config;

    if (method.toUpperCase() === 'GET') {
        config.params = config.data
    }

    return request.request<CustomResponse<T>>(config);
}

// 取消请求
const cancelRequest = (url: string | string[]) => {
    return request.cancelRequest(url);
}

// 取消全部请求
const cancelAllRequest = () => {
    return request.cancelAllRequest();
}

export default customRequest;
export {
    cancelRequest,
    cancelAllRequest
}