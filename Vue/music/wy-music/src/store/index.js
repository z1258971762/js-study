import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    songInfo: {
      // 歌曲信息
      albUrl: '',
      // 歌曲名称
      songName: '',
      // 艺术家名字
      artName: '',
    }
  },
  mutations: {
    setSongInfo(state, payload = {}) {
      state.songInfo = payload;
    },
  },
  getters: {
    albUrl(state) {
      // return state.songInfo
      return state.songInfo.albUrl;
    }
  },
  actions: {
  },
  modules: {
  }
})
