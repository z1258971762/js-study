// pages/localLife/localLife.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 存放轮播图数据列表
    swiperList:[],
    // 存放九宫格的数据列表
    gridList:[]
  },

  /**
   * 获取轮播图列表数据的方法
   */
  getSwipper(){
    let self = this;
    wx.request({
      url: 'http://localhost:3000/api/get/swippers',
      method:'GET',
      success(res){
        console.log(res);
        // 这里直接用this会丢失。这里注意一下
        self.setData({
          swiperList: res.data
        })
      }
    })
  },
  /**
   * 获取九宫格列表
   */
  getGrid(){
    wx.request({
      url: 'url',
      method:'GET',
      success:()=>{
        this.setData({
          gridList: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getSwipper();
    this.getGrid();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})