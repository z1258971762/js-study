// 请补全JavaScript代码，要求实现一个对象参数的浅拷贝并返回拷贝之后的新对象。
// 注意：
// 1. 参数可能包含函数、正则、日期、ES6新对象
// 对于域浅拷贝
// 对于基础数据类型数据直接拷贝
// 对于引用类型数据仅拷贝第一层对象的属性，重新开辟一个地址将其存储（深拷贝和浅拷贝的重要差异）
const _shallowClone = target => {
    // 补全代码
    // 基本数据类型
    if(typeof target !== 'object' || target === null) return target;
    let ret = Array.isArray(target)?[]:{};
    // Object.assign(ret, target);
    let str = Object.prototype.toString.call(target);
    // 函数，日期正则，直接返回
    if(str == '[object Date]' || str == '[object RegExp]' || str == '[object Function]')
    return target;

    Object.keys(target).forEach(key=>{
        ret[key] = target[key];
    })
    return ret;
}


let obj = {
    a: 123,
    b: 345,
    c:{
        d:[1,2,34,5]
    }
}
let obj2 = _shallowClone(obj);
obj.c.d.push(564)
console.log(obj2);
