// 封装一下localstorage，使其具有设置失效时间的能力
// 这里采用的是封装为class 的方式
class Storage {
    constructor(name) {
        this.name = name || 'storage';
    }

    setItem(params) {
        let obj = {
            name: '',
            value: '',
            expires: '',
            startTime: new Date().getTime()
        }

        let options = {};

        // 将obj和传入的param进行合并
        Object.assign(options, params, obj)
        if (options.expires) {
            localStorage.setItem(options.name, JSON.stringify(options))
        } else {
            // 如果没有设置过期时间，那就看看是不是对象， 对象的话，就序列化一下
            if (typeof options.value === 'object') {
                options.value = JSON.stringify(options.value);
            }
            localStorage.setItem(options.name, options.value);
        }
    }

    getItem(key) {
        let item = localStorage.getItem(key);

        try {
            item = JSON.parse(item)
        } catch (err) {
            // 不是JSON， 那就是字符串
            item = item;
        }
        if (item.expires) {
            // 有失效时间，那么就看一下时间
            let now = new Date().getTime();

            if (now - item.startTime > item.expires) {
                localStorage.removeItem(item.name);
                return null;
            } else {
                return item.value;
            }
        } else {
            return item;
        }
    }

    removeItem(name) {
        localStorage.removeItem(name);
    }

    clear() {
        localStorage.clear();
    }
}


let storage = new Storage();
storage.setItem({
    name: 'test-key',
    value: 'test11111'
})