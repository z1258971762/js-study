// 请补全JavaScript代码，完成"EventEmitter"类实现发布订阅模式。
// 注意：
// 1. 同一名称事件可能有多个不同的执行函数
// 2. 通过"on"函数添加事件
// 3. 通过"emit"函数触发事件
class EventEmitter {
    // 补全代码
    constructor() {
        this.events = Object.create(null);
    }
}

EventEmitter.prototype.on = function (event, callback) {
    if (!this.events[event]) {
        this.events[event] = [];
    }
    this.events[event].push(callback);
}

EventEmitter.prototype.emit = function (event, ...args) {
    if (!this.events[event]) {
        return;
    }
    this.events[event].forEach(fnc => {
        fnc(...args)
    })
}
/**
 * 只订阅一次
 * @param {事件名称} event 
 * @param {回调} callback 
 */
EventEmitter.prototype.once = function (event, callback) {
    let temp = (...args) => {
        callback(...args);
        this.off(event, temp);
    }
    this.on(event, temp);
}
/**
 * 移除事件
 * @param {事件名称} event 
 */
EventEmitter.prototype.off = function (event, callback) {
    if (!this.events[event]) return;
    this.events[event] = this.events[event].filter(item => item != callback);
}
let ev = new EventEmitter();
ev.on('abc', (param) => { console.log('123', param); })
ev.on('abc', (param) => { console.log('456', param); })
ev.once('abc', (param) => { console.log('789', param); })
ev.emit('abc', 'rtyuy');
ev.emit('abc', 'yiuoi');