import createRoute from "../util/route";
export default class History {
    constructor(router) {
        this.router = router;
        // 当前路由
        this.current = createRoute(null, '/');
        this.cb = null;
    }
    /**
     * 
     * @param {修改VM上的_route属性值} cb 
     */
    listen(cb){
        this.cb = cb;
    }

    /**
     * 
     * @param {跳转路径} path 
     * @param {完成后的回调函数} onComplete 
     */
    transtionTo(path, onComplete) {
        // 将当前路由改为匹配的路由
        // 为什么可以调用matcher.match 因为在创建VueRouter 的时候，初始化了这个属性。
        this.current = this.router.matcher.match(path);
        // 调用更改current的方法
        this.cb && this.cb(this.current);
        onComplete && onComplete();
    }
}