/**
 * 最终版本的封装
 * 此版本较先前的两个版本更加灵活，结构更加清晰
 * 封装了循环方法
 * 同时使导出的JQuery更纯净
 * 还有链式调用
 */
let $ = JQuery = (function (window) {
    // DOM 存储
    function Query(dom, selector) {
        let len = dom ? dom.length : 0;
        for (let i = 0; i < len; i++) {
            this[i] = dom[i];
        }
        this.length = len;
        this.selector = selector || '';
        return this;
    }
    /**
     * 生成jquery对象
     * 为什么有他？因为可以让JQuery的原型更加纯净
     * @param {dom元素} elements 
     * @param {选择器} selector 
     * @returns 
     */
    function GenJQ(elements, selector) {
        return Query.call(this, elements, selector);
    }

    /**
     * 具体的查询操作
     * @param {元素} element 
     * @param {选择器} selector 
     */
    function qsa(element, selector) {
        return element.querySelectorAll(selector);
    }

    GenJQ.prototype = {
        each(callback) {
            [].every.call(this, function (el, index) {
                return callback.call(el, el, index) !== false;
            })
        },
        find(selector) {
            let doms = [];
            this.each(function (el, index) {
                let childs = this.querySelectorAll(selector);
                doms = [...childs];
            })
            return new GenJQ(doms, selector);
        },
        eq(i) {
            let doms = [];
            this.each(function (el, index) {
                if (index == i) {
                    doms.push(this);
                }
            })
            return new GenJQ(doms, this.selector);
        },
        remove() {
            this.each(function (el, index) {
                this.remove();
            })
        },
        addClass(classes) {
            let className = classes.split(' ');
            className.forEach(val => {
                this.each(function (el, index) {
                    el.classList.add(val);
                })
            })
            return this;
        },
        setText(text) {
            this.each(function (el, index) {
                el.textContent = text;
            })
            return this;
        }
    }
    /**
     * 全局方法  判断是否是函数
     * @param {函数} val 
     * @returns 
     */
    function isfunction(val) {
        return typeof val === 'function'
    }

    function $(selector) {
        let doms = qsa(document, selector);
        return new GenJQ(doms, selector);
    }

    $.isfunction = isfunction;

    return $;

})(window)