import axios from 'axios'

// 设置baseURL
axios.defaults.baseURL = 'http://localhost:3000';

export default axios;