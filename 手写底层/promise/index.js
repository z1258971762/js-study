function MyPromise(executor) {
    // 1、执行的基本结构
    let self = this;

    self.status = 'pending'; // 状态
    self.value = null; //成功的结果
    self.reason = null; //失败的原因
    // 7、异步失败，则添加数组，将then方法中的回调缓存
    self.successCallBack = [];
    self.failCallBack = [];
    // 4、判断状态，做相应的处理
    // 成功回调
    function resolve(value) {
        if (self.status === 'pending') {
            // 保存成功结果
            self.value = value;
            self.status = 'fulfilled';
            // 9、依次取出执行
            self.successCallBack.forEach(fn => fn(value))
        }
    }
    // 失败回调
    function reject(resaon) {
        if (self.status === 'pending') {
            // 保存失败原因
            self.reason = resaon;
            self.status = 'rejected';
            // 9、依次取出执行
            self.failCallBack.forEach(fn => fn(resaon))
        }
    }

    // 3、执行一下函数
    try {
        executor(resolve, reject)
    } catch (e) {
        console.log('executor执行出错了');
        reject(e);
    }
}

// 2、then 方法
MyPromise.prototype.then = function (onFulfilled, onRejected) {
    // 5、状态改变调用then方法
    onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : function (data) { resolve(data) };
    onRejected = typeof onRejected === 'function' ? onRejected : function (err) { throw err };

    if (this.status === 'fulfilled') {
        onFulfilled(this.value);
    } else if (this.status === 'rejected') {
        onRejected(this.reason);
    } else {
        // 8、pending状态，缓存起来
        this.successCallBack.push(onFulfilled);
        this.failCallBack.push(onRejected);
    }
}

let prom = new MyPromise((resolve, reject) => {
    console.log('12343242');
    // resolve('OK;e ')
    // 6、尝试异步--异步失败
    setTimeout(() => {
        resolve(234)
    }, 500)

}).then((res) => {
    console.log('res', res);
})