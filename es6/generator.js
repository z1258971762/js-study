/**
 * 定义生成器
 */
function *generator(){
    let a = yield 'node';
    console.log(a);
    let b = yield 'vue';
    console.log(b);
}
// 返回一个迭代器
let itrator = generator();
console.log(itrator.next('abc'));
console.log(itrator.next('bcd'));
console.log(itrator.next('cde'));


// 利用generator来实现数字的累加操作
//下一次next的参数，是上一次yield的返回值
//这个例子看起来可能没啥用，但是如果换成异步操作，就相当于把所哟的异步操作变成了同步执行
function *addNum(){
    let sum = 0;
    let a = yield sum;
    console.log('a',a);
    let b = yield sum += a;
    let c = yield sum += b;
    return sum += c;
}
let ite = addNum();
console.log(ite.next(100));
console.log(ite.next(200));
console.log(ite.next(300));
console.log(ite.next(400));