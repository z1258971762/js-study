// 请补全JavaScript代码，要求通过寄生组合式继承使"Chinese"构造函数继承于"Human"构造函数。要求如下：
// 1. 给"Human"构造函数的原型上添加"getName"函数，该函数返回调用该函数对象的"name"属性
// 2. 给"Chinese"构造函数的原型上添加"getAge"函数，该函数返回调用该函数对象的"age"属性

/**
 * 寄生组合继承
 * 盗用构造方法
 * 继承原型对象
 * 更改自己的constructor
 */

// 补全代码
function Human(name) {
    this.name = name
    this.kingdom = 'animal'
    this.color = ['yellow', 'white', 'brown', 'black']
}

function Chinese(name, age) {
    Human.call(this, name);
    this.color = 'yellow'
    this.age = age;
}

/**
 * 实现继承对象
 * @param {继承的对象} proto 
 */
function extendObj(proto) {
    function fn() { }
    Object.setPrototypeOf(fn.prototype, proto);
    return new fn();
}

Human.prototype.getName = function () {
    return this.name;
}

Chinese.prototype = extendObj(Human.prototype);
Chinese.prototype.constructor = Chinese;
Chinese.prototype.getAge = function () {
    return this.age;
}

function fn() { 
    const o = new Chinese('z', 18); 
    const judge = o.getAge() === 18 && o.getName() === 'z' && o.kingdom === 'animal' && o.__proto__.constructor === Chinese; 
    return judge 
}
console.log(fn());
// let chn = new Chinese('zx', 18);
// console.log(chn.getName());
// console.log(chn.getAge());
