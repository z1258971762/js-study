import Vue from 'vue'
// import VueRouter from 'vue-router'
// import VueRouter from '../my-router'
import VueRouter from '../router-advance'
import Home from '../components/Home.vue'
import Login from '../components/Login.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    component:Login
  },{
    path: '/A',
    name: 'A',
    component: ()=> import('../components/A.vue'),
    children:[
      {
        path: '/B',
        name: 'B',
        component: {
          render(h){
            return h('p',{},['这是动态的'])
          }
        }
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
