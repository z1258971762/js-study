import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home'
import Search from '@/views/Search'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/layout'
  },
  {
    path: '/layout',
    name: 'layout',
    component: () => import('@/views/Layout'),
    redirect: '/layout/home',
    children: [
      {
        path: 'home',
        name: 'home',
        component: Home,
        // 定义额外信息，定义标题
        meta: {
          title: '首页'
        }
      },
      {
        path: 'search',
        component: Search,
        // 定义额外信息，定义标题
        meta: {
          title: '搜索'
        }
      },
    ]
  },
  {
    path: '/playSong',
    component: () => import('@/views/play/index.vue'),
    name: 'playSong',
    meta: {
      title: '播放音乐'
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
