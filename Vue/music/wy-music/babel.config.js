module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  // 自动按需引入组件 (推荐)--引入vant的CSS
  // 详情/：https://vant-contrib.oschina.io/vant/#/zh-CN/quickstart#fang-shi-yi.-zi-dong-an-xu-yin-ru-zu-jian-tui-jian
  plugins:[
    ['import', {
      libraryName: 'vant',
      libraryDirectory: 'es',
      style: true
    }, 'vant']
  ]
}
