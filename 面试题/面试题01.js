(function () {
    // 阿里面试
    var length = 10;
    function test() {
        console.log(this.length);
    }

    var obj = {
        length: 100,
        action: function (test) {
            test();
            arguments[0]();
        }
    }

    obj.action(test, 1, [2, 4], 4);
})()



// 结果：
// undefined 和 4  undefined 是因为 全局上没有length，因为是自执行函数，所以length = 10 并没有在全局上。而4 是应为相当于arguments.0() this 指向 arguments

window.onload = function () {
    // 阿里面试
    var length = 10;
    function test() {
        console.log(this.length);
    }

    var obj = {
        length: 100,
        action: function (test) {
            test();
            arguments[0]();
        }
    }

    obj.action(test, 1, [2, 4], 4);
}
// 结果： 10 ， 4

// this规则  执行时，看有没有调用者，没有调用者，指向window，如果有调用者，那么指向调用者
// 普通函数：
// 直接调用的函数，this指向window
// 对象.函数的形式执行，this 指向当前对象
// 数组中的元素是函数，执行的时候，指向的是这个数组（其实相当于打点的方式执行，obj.name = obj[name],所以this指向的是当前的arr）
var length = 100;
function test(){
    console.log('this',this);
    console.log(this.length)
}
var arr = [test,1,3,4];
arr[0](); // 4

// 箭头函数不识别 arguments 会报错 arguments is not defined
let fn = ()=>{
    console.log(11111,arguments);
}
fn(1,2,3,4)


/***************第二题************* */
// 作用域
// 预解析 解析 var 和function 会提前
var a = 10;
function test1(){
    console.log(a);
    a = 100;
    console.log(this.a);
    var a;
    console.log(a);
}
test1();
// undefined 10 100