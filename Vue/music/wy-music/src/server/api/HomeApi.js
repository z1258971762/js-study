import axios from "../http";

// 首页推荐歌单
const recommendMusic = params => axios({
    url: '/personalized',
    params
})

// 最新音乐
const latestMusic = params => axios({
    url: '/personalized/newsong',
    params
})

// 获取歌曲的URL
const getSongUrl = id => axios({
    method: 'GET',
    url: '/song/url',
    params: {
        id
    }
})

export {
    recommendMusic,
    latestMusic,
    getSongUrl
}