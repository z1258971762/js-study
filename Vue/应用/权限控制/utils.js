/**
 * 返回当前权限是否在授权的权限中
 * @param {校验的权限} permit 
 * @returns 
 */
export function checkPermission(permit){
    // 假设这个是后端查询出来的
    let arr = [1,3,5,7,9];
    return arr.includes(permit);
}