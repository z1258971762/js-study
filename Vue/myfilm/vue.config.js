// 对整个项目进行配置，因为基于webpack，所以可以配置webpack
// 引入postcss
const pxtovm = require('postcss-px-to-viewport');
// 引入path模块
const path = require('path')
module.exports = {
    css: {
        loaderOptions: {
            css: {
                // 这里的配置会传递给css-loader
            },
            postcss: {
                // postcss配置一个组件：
                plugins: [
                    new pxtovm({
                        // 这里的选项会传递给postcss-loader
                        unitToConvert: 'px',//要转换的单位
                        viewportWidth: 375,//视口宽度--注意会影响转换后的大小
                        unitPrecision: 5,//保留5位小数
                        propList: ['*'],// 单位是PX的都转换，配置哪些属性进行转换
                        viewportUnit: 'vw', // 视口单位
                        fontViewportUnit: 'vw', // 字体单位
                        selectorBlackList: [], // 不进行转换的
                        minPixelValue: 1, // 最小像素值
                        mediaQuery: false, // 媒体查询不转换
                        replace: true, //
                        exclude: [/node_modules/],// 哪些不转换--第三方模块不转换 
                    })
                ]
            }
        }
    },
    // 通过链式的方式配置webpack,也可以不用链式，直接使用对象的形式配置
    chainWebpack: config =>{
        // 给路径起一个别名。到时候使用的时候，直接用assets就行了
        config.resolve.alias.set('assets', path.resolve(__dirname,'/src/assets/'))
    }
}