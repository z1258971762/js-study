/********************流的使用****************/
const fs = require('fs');
const path = require('path');
/*********可读流*--基于发布订阅**********/
let readStream = fs.createReadStream(path.join(__dirname, './acop.txt'), {
    flags: 'r',//读还是写
    encoding: null, //如果为空是二进制文件 default buffer
    mode: 0o666,
    fd: null,//文件描述符
    autoClose: true,//是否自动关闭
    emitClose: false,//读完以后是否自动触发关闭事件
    start: 0, //开始的偏移量
    end: 10,//结束的偏移量--默认无穷大，也就是从头到尾
    highWaterMark: 3,//每次读取的偏移量
    // start 和end都是包含的
});
// 监听文件打开
readStream.on('open', (fd) => {
    console.log('文件被打开了：', fd);
})
// 监听文件读取，默认是暂停模式。不监听这个事件，是不会读的，也不会关闭
// 监听之后，数据变为流动的。此方法会被调用多次
let buffer = [];
readStream.on('data', (buf) => {
    // console.log('文件：',buf);
    buffer.push(buf);
})
// 监听文件读取完毕
readStream.on('end', () => {
    console.log('拼接文件：', Buffer.concat(buffer).toString());
})

// 监听文件关闭
readStream.on('close', () => {
    console.log('file closed');
})
/**
 * 监听错误
 */
readStream.on('error', (err) => {
    console.log('出错了：', err);
});


// 使用自己写的ReadStream
const ReadStream = require('./ReadStream');

let rs = new ReadStream(path.join(__dirname, './aco0py.txt'), {
    highWaterMark: 6,
    start: 0,
    flags: 'r'
});

rs.on('open', (fd) => {
    console.log('文件被打开了：', fd);
})
let cacheBuffer = [];
rs.on('data', (buf) => {
    cacheBuffer.push(buf);
});
rs.on('end', () => {
    console.log('拼接文件：', Buffer.concat(cacheBuffer).toString());
});

rs.on('close', () => {
    console.log('file closed');
});
rs.on('error', (err) => {
    console.log('出错了：', err);
});



/*******************可写流*********************/
let ws = fs.createWriteStream(path.resolve(__dirname, './writeStream.txt'), {
    flags: 'w',
    encoding: 'utf8',
    mode: 0o666,
    autoClose: true,
    start: 0,
    highWaterMark: 2,//默认16*1024；
});

// 文件打开
ws.on('open', () => {
    console.log('file open');
});

// flag使用、往文件里面写10个数字

let index = 0;

function write() {
    let writeFlag = true;
    // 这样可以实现写入暂停
    while (writeFlag && index != 10) {
        writeFlag = ws.write(++ index + '');
    }
}
// 当写入的内容达到预估量标准highwatermark,并且数据在内存中清空以后，触发
ws.on('drain', function () {
    // 达到预估值
    console.log('-----------达到预估值---------');
    write();
});
write();

// 不知道为什么？需要注释调下面的代码，才能打印达到预估值

// 写入文件
ws.write('hello', function () {
    console.log('写入成功');
});
// 写入虽然是异步的，但是会顺序执行
let flag = ws.write('word', function () {
    console.log('写入成功');
});
console.log('写方法会返回一个flag，累计写入的内容超过或者等于预估值highwatermark就返回false，否则返回true：', flag);


// 写入结束,写入结束之后，不可以再调用write，会抛错误
ws.end('这是写入的遗言，也会写入文件');
// 文件关闭
ws.on('close', () => {
    // 不会主动执行，必须调用ws.end();后才会执行
    console.log('file closed');
});

/***************测试自己的可写流***************/
const path = require('path');
const WriteStream = require('./WriteStream');
let write = new WriteStream(path.join(__dirname, './MyWriteStream.txt'), {
    start: 0,
    highWaterMark: 2,//默认16*1024；
});

write.on('open', () => {
    console.log('self file opend');
});
write.on('drain', () => {
    console.log('-----------达到预估值---OK------');
});
write.on('error', (e) => {
    console.log('出错了：', e);
})
write.write('h', 'utf8', () => {
    console.log('write h OK');
});
write.write('hello','utf8',()=>{
    console.log('write hello OK');
});
