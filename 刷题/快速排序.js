const _quickSort = array => {
    // 补全代码
    if(!array || array.length == 0) return [];
    console.log(array);
    let start = 0;
    let end = array.length-1;
    let key = array[start];
    
    while(start < end){
        // 从右到左寻找比基小的
        while(start < end && key < array[end]){
            end --;
        }
        // 交换数值
        if(start < end){
            array[start ++] = array[end];
        }
        // 从左到右寻找比基准大的
        while (start < end && key > array[start]) {
            start ++;
        }
        if(start < end){
            array[end --] = array[start];
        }
    }
    // 退出时，start == end 把基准值放在这里
    array[start] = key;
    // 递归调用
    // console.log('start',start,'end',end);
    return [..._quickSort(array.slice(0,start)),array[start],..._quickSort(array.slice(start+1))];
}

let arr = [47,29,71,99,78,19,24,47];
console.log(_quickSort(arr));
// console.log(arr.slice(0,5));
// console.log(arr.slice(6));