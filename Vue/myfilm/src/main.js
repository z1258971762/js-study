import Vue from 'vue'
// import App from './App.vue'
import App from './index.vue'
import router from './router'
import store from './store'
// // 导入vant
// import Vant from 'vant'
// // 引入vant所有组件样式
// import 'vant/lib/index.css'
// // 全局注册vant所有的组件
// Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
