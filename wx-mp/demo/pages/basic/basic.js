// pages/basic/basic.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    msg:'hello',
    inputVal:'word',
    showView:true,
    fruits:['🍎','banana','orange']
  },

  btnHandler(e){
    // 获取按钮传参
    console.log(e.target.dataset.a);
  },
  // 输入处理函数
  inputHandler(event){
    // 获取input输入的东西
    console.log(event.detail.value);
    // 设置值
    this.setData({
      inputVal:event.detail.value
    })
  },

  /**
   * 控制条件渲染条件
   */
  showViewControl(){
    this.setData({
      showView: !this.data.showView
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})