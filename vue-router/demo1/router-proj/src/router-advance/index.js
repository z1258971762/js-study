import install from './install';
import {createMatcher} from './create-matcher'
import HashHistory from './history/hash';
import HTML5History from './history/html5';
export default class VueRouter{
    constructor(options){
        this.options = options;
        // 存放路由
        this._routes = options.routes;
        this.matcher = createMatcher(options.routes || [],this)
        const mode = this.mode = options.mode || 'hash';
        switch (mode) {
            case 'hash':
                this.history = new HashHistory(this);
                break;
        
            case 'history':
                this.history = new HTML5History(this);
                break;
            default:
                throw new Error('mode error');
        }
    }
    // 初始化方法
    init(vm){
        const history = this.history;
        // 默认跳转到当前路由地址 #   注意这里为什么用一个箭头函数包起来，因为setUpListener 里面用到了this。所以这里要保证，里面的this是history实例，当然也可以不用回调函数，直接调用
        history.transtionTo(history.getCurrentLocation(),()=>{history.setUpListener()});
        history.listen((route)=>{
            vm._route = route;
        })
    }
    // 挂载install方法
    static install = install;
}

// 挂载install方法
// VueRouter.install = install;