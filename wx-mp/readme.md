## 微信小程序简介

### 微信小程序与网页的开发

#### 运行环境

小程序基于微信环境、普通网页基于浏览器环境

#### 宿主环境API
小程序宿主是微信，而普通基于浏览器，所以API不同

网页使用**DOM、BOM**
小程序 可以调用微信宿主环境提供的API能力（地理定位、微信支付...）

#### 开发模式
> 小程序需要：开发账号、用官方推荐的开发者工具、创建和配置小程序项目



## 全局配置
### app.json 是小程序中的全局配置文件
> pages 放的是小程序的所有页面，第一个是首页
> **window** 全局设置的是所有窗口的外观
> style 表示是否采用新版组件样式，默认值是启用的V2，删掉则默认是V1
> sitemaplocation  指定的是sitemap.json的路径(相对路径)
> **tabBar**  设置小程序底部的tabBar的底部效果

### 小程序窗口的组成部分
+ 导航栏区  navigationBar
+ 背景区域  background
+ 页面主体区域

### window节点常用配置项
+ navigationBarTitleText:"导航栏的名字"
+ navigationBarBackgroundColor:"导航栏区域的背景颜色"（不支持单词，只能写16进制颜色）
+ navigationBarTextStyle: "导航栏字体样式，只能是黑白"（不支持十六进制）
+ enablePullDownRefresh:"是否允许下拉刷新，true/false"(默认是不会允许的)
+ backgroundColor: "#ffffff" ( 仅支持十六进制，改变下拉刷新时的背景颜色)
+ backgroundTextStyle:"下拉字体颜色" （仅支持dark、light）
+ onReachBottomDistance  "页面上拉触底的距离触发刷新，单位PX"（相当于时页面距离底部还有多少的时候触发，滚动条距离底部有多高的时候触发，配合页面上的onReachBottom方法使用）

### TabBar的常用配置

一种是底部的tabBar，另一种是顶部的tabBar

**注意⚠️：**最少配置2个tab页签，最多配置5个。一般顶部的tabbar，图标不会显示，只会显示文字。

+ "color": "#000000", 未选中的字体颜色
+ "selectedColor": "#000000",  选中的字体颜色
+ "list" 配置tanbar的详细信息
  + "pagePath": 页面路径
  + "text": tabBar文字
  + iconPath 未选中的图标路径
  + selectedIconPath  选中的图标路径
+ backgroundColor 背景颜色
+ borderStyle  边框颜色，black/white
+ position 位置，top/bottom

## 页面配置

每个页面都有自己的json配置文件，用来配置当前页面的窗口外观。

每个页面的JSON就不需要写window这个属性了，直接写下面的`navigationBarTitleText`就好了。以局部的为准

## 网络请求

小程序对数据接口的请求需要满足条件：

+ 只能够请求Https类型的接口。
+ 请求的域名，必须在后台有配置。必须添加到信任列表

**注意⚠️：**

+ ip地址或者localhost都不能使用
+ HTTPS接口才能访问
+ 域名必须经过ICP备案
+ 服务器域名，一月最多可申请修改5次

### 跳过request合法域名校验

点击详情、本地设置、勾选不校验合法域名（只能在开发阶段使用）

小程序不存在跨域问题。因为他依赖于微信，而不是浏览器，是浏览器的同源策略导致的跨域问题。



## 页面事件

### 下拉刷新

默认不开启，开启方式，全局在app.json的window节点增加即可。局部的在自己页面的节点中，直接写`enablePullDownRefersh: true即可

**设置下拉刷新的样式**

`backgroundColor:#16进制颜色` 背景颜色

`backgroundTextStyle:dark/white` 下拉刷新的字体颜色

**监听用户的下拉刷新**

`onPullDownRefresh` 监听用户的下拉刷新事件，然后在里面做一些操作

**注意⚠️**：处理完了下拉刷新以后，loading效果会一直显示。此时需要主动去停止当前页面的下拉刷新。此时调用方法`wx.stopPullDownRefresh();`停止刷新

### 上拉触底

默认上拉刷新的时候，是50px的距离。当到达设置的距离底部的距离的时候，会触发一个事件：`onReachBottom`在这个函数里面，一般会在此方法中进行分页操作。

**如何设置上拉触底的距离？**

一般来说，全局是在window节点下配置，`onReachBottomDistance` 。局部的就在自己的**.json**文件去修改。直接写就可

## 生命周期

生命周期是一个从出生到死亡的过程（创建--》运行--〉销毁）

### 小程序应用的生命周期

指的是小程序从启动到运行到销毁的过程

### 页面的生命周期

小程序的页面，从加载-->渲染-->销毁的过程

### 生命周期函数

在不同的时期，所要执行的函数。自动按照顺序去执行。

有应用的生命周期函数和页面的生命周期函数

#### 应用的生命周期

应用的生命周期在全局的**app.js**中定义

+ onLaunch 小程序初始化后做一些初始化的操作
+ onShow。小程序切换前台的时候展示。也就是展示的时候
+ onHide. 小程序切换到后台时运行。也就是隐藏的时候

#### 页面的生命周期

+ onLoad。页面加载时调用，可以在此拿到当前页面的启动参数，或者其他页面带的参数
+ onshow。页面展示的时候调用
+ onReady。初次渲染完成后调用，可以在这里设置导航标题（wx.setNavigationBarText）

## WXS

### 应用场景

一般应用在页面之内，用来处理展示数据的。相当于Vue的过滤器

### 与JS的一些比较

WXS： number、boolean、string、object、function、arr、date、regexp

不支持ES6及以上的语法，比如let，const，解构赋值，展开运算符

支持CommonJS规范（module.export）

### 使用

在wxml中增加**wxs**的标签

#### 内嵌式

注意事项：

+ 一定要在wxs的标签上增加module的属性，用来起一个名字如：

  ```wxml
  <!--使用-->
  <view>{{m1.toUpper(str)}}</view>
  <!--定义-->
  <wxs module="m1">
  	module.exports.toUpper = function toUpper(str){
  		return str.toUpperCase();
  	}
  </wxs>
  ```

+ 一定要使用commonJS规范将函数导出

+ 导出的名字与使用的名字保持一致即可

#### 外链式

外链式一般在外部定义一个文件，并且将函数导出。然后在使用的页面引入，使用方式保持一致。

使用时注意事项：

+ 导出的时候不能简写，一定要采用键值对的形式导出。因为不支持ES6语法
+ 使用的时候通过src的**相对路径**引入文件
+ 名字还是要起的，module="m2"
+ 使用方式与内嵌一致

```wxs
function toLower (str){
	return str.toLowerCase();
}

//导出
module.exports = {
	toLower:toLower
}

//引入
<wxs src ="../../utils/util.wxs" module="m2"></wxs>

//使用
<view>{{m2.toLower(str)}}</view>
```

### 特点

+ WXS中定义的函数，不能作为回调使用，比如按钮的**bindtab**
+ WXS具有隔离性，不能使用其他JS中的函数方法
+ 也不能够调用小程序的静态API
