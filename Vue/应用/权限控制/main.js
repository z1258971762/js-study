import Vue from 'vue';
import App from './App.vue';
import { checkPermission } from './utils'

Vue.directive('permission', {
    inserted(el, binding) {
        let key = binding.value;

        if (key) {
            let hasKey = checkPermission(key);
            if (!hasKey) {
                el.parentNode && el.parentNode.removeChild(el);
            }
        } else {
            throw new Error('指令需要一个权限')
        }
    }
})