# myfilm

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).





### 记录一下使用的插件
> **postcss-px-to-viewport**  将px转换为vw，做移动端适配用的。或者也可以转换为rem，那就要使用另一个插件**postcss-pxtorem**

安装方法：

```
npm install postcss-px-to-viewport --save-dev
开发依赖--转换完了就行了
``` 

```
npm install postcss postcss-pxtorem --save-dev
```
使用方法：在**vue.config.js**中进行配置