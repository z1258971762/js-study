import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
    {
        path: '/treeView',
        component: () => import('../officalDemo/treeView.vue')
    },
    {
        path: '/grid',
        component: () => import('../officalDemo/filterGrid.vue')
    }, {
        path: '/transModel',
        component: () => import('../officalDemo/transModel.vue')
    },
    {
        path: '/todo',
        component: () => import('../officalDemo/todoMvc.vue')
    }
]
const router = createRouter({
    history: createWebHashHistory(),
    routes
});
export default router;