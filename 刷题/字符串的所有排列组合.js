// 1. 字符串参数中的字符无重复且仅包含小写字母
// 2. 返回的排列组合数组不区分顺序
// 输入：_permute('abc')
// 输出：['abc','acb','bac','bca','cab','cba']

const _permute = string => {
    // 补全代码
    // 解题思路
    // ‘abc’的全排列等于 ('a'拼接上'bc'的全排列数组中的每一项) + ('b'拼接上'ac'的全排列数组的每一项) + ('c'拼接上'ab'的全排列数组的每一项)
    if(string.length == 1){
        return [string];
    }

    let ret = [];
    let arr = string.split('');

    for(let index = 0 ; index < arr.length; index++){
        let array = [...arr];
        let char = array.splice(index,1);
        // 递归调用方法
        _permute(array.join('')).forEach(item=>{
            ret.push(char + item);
        })
    }
    return ret;
}


// 一个一个进行拼接
// const _permute = string => {
//     const search = path => {
//         if (path.length === string.length) {
//             res.push(path);
//             return;
//         }
//         for (const char of string) {
//             if (path.indexOf(char) < 0) {
//                 search(`${path}${char}`);
//             }
//         }
//     };
//     const res = [];
//     search('');
//     return res;
// };

// const _permute = string => {
//     var res = []
//     function search(path) {
//         path.length == string.length && res.push(path);
//         for (const char of string) {
//             path.indexOf(char) < 0 && search(`${path}${char}`);
//         }
//     }
//     return search('') || res;
// };


console.log(_permute('rapc'));