// npm i body-parser ---解析body数据
// npm i express
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// 搭建静态服务器
app.use(express.static('public'))
// 解析JSON数据 application/json
app.use(bodyParser.json());
// 解析表单 application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// 设置允许跨域
app.all('*', function (req, res, next) {
    // 设置允许跨域的域名
    //自定义中间件，设置跨域需要的响应头。
    res.header('Access-Control-Allow-Origin', '*');
    //允许任何方法
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    //允许任何类型
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,X-Session-Token');
    //一定要添加这段代码，不然程序无法正常往下执行，看着像一个filterChain
    next();
});

// 设置请求路径
app.get('/data', (req, res) => {
    res.send("hello ajax");
})

app.get('/async/data', (req, res) => {
    setTimeout(res.send("hello ajax"), 1000)
})

app.get('/data2', (req, res) => {
    res.send('ajax2')
})

// 给fetch准备的接口
app.get('/fdata', (req, res) => {
    res.send('fdata--hello--fetch')
})

app.get('/news', (req, res) => {
    res.send(req.query.id)
})

// restful方式
app.get('/news', (req, res) => {
    res.send(req.param.id)
})

// post
app.post('/news', (req, res) => {
    res.send('a:'+ req.body.a + '---b:',req.body.b)
})

// JSON   请求
app.post('/news2', (req, res) => {
    console.log(typeof req.body);
    res.send('a:'+ req.body.a + '---b:',req.body.b+ '这个是JSON')
})


// Axios访问的地址
app.get('/axiosData',(req,res)=>{
    res.send('hello axios');
})

// axios 传统GET
app.get('/axiosGet',(req,res)=>{
    res.send(req.query.id);
})
// axios restful get
app.get('/axiosRestfulGet:id',(req,res)=>{
    res.send(req.params.id);
})

app.listen(3000, () => {
    console.log('server start');
});

// 使用nodemon来使服务器自动重启