/**
 * 字符串转BufferArray
 * @param {目标字符串} str 
 */
function strToBufferArray(str) {
    let buffer = new ArrayBuffer(str.length * 2);
    for (let index = 0; index < str.length; index++) {
        let view = new Uint16Array(buffer);
        view[index] = str.charCodeAt(index);
    }
    return buffer;
}

/**
 * buffer转换为字符串
 * @param {buffer数组} buffer 
 */
function bufferArrayToStr(buffer) {
    return String.fromCharCode(...new Uint16Array(buffer));
}

console.log('zhuanhua',strToBufferArray('中国'));