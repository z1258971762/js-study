import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

// 引入自己的页面
import MyHome from '../views/myhome.vue'
import City from '../views/city.vue'
import Theater from '../views/theater/theater.vue'
import Profile from '../views/profile/profile.vue'
import HotShowing from '../views/movie/hotshow.vue'
import CommingSoon from '../views/movie/comingsoon.vue'

Vue.use(VueRouter)

const routes = [
  // 定义自己的路由
  {
    path: '/',
    redirect: '/index/movies/hotShow'
  },
  {
    path: '/index',
    name: 'MyHome',
    redirect :'index/movies/hotShow',
    component: MyHome,
    children: [
      {
        path: 'movies',
        name: 'movies',
        redirect: 'movies/hotShow',
        // 导入组件的另一种方式 -- 这里一定要return，粗心了，大意了
        component: () => { return import('../views/movie/movie.vue') },
        children: [
          {
            path: 'hotShow',
            name:'hotShow',
            component: HotShowing
          },
          {
            path: 'commingSoon',
            component: CommingSoon
          }
        ]
      },
      {
        path: 'theater',
        component: Theater
      },
      {
        path: 'profile',
        component: Profile
      }
    ]
  },
  {
    path: '/city',
    name: 'City',
    component: City
  },

  {
    path: '/vueHome',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
